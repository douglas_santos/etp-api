package staff

import (
	"context"
	"math/rand"
	"time"
)

type fakeRepository struct {
	lastStaffId    uint64
	lastTrainingId uint64
	lastSearchId   uint64
	trainings      map[uint64]*Training
	staff          map[uint64]map[uint64]*Staff
	searches       map[uint64]map[uint64]*Search
}

func NewFakeRepository() Repository {
	staff := map[uint64]map[uint64]*Staff{
		2: {
			1: {
				Id:       1,
				Name:     "John Doe",
				Skill:    52,
				Talent:   50,
				Salary:   1523400,
				Status:   HIRED,
				Offer:    2000000,
				Employer: 2,
				HiredAt:  time.Now().Add(-time.Hour),
			},
		},
		1: {
			2: {
				Id:       2,
				Name:     "Jane Doe",
				Skill:    52,
				Talent:   50,
				Salary:   2000000,
				Status:   HIRED,
				Employer: 1,
				HiredAt:  time.Now().Add(-time.Hour),
			},
		},
	}

	now := time.Now()

	trainings := map[uint64]*Training{
		1: {
			Id:          1,
			Result:      0,
			Investment:  10000000,
			StaffId:     2,
			CompanyId:   1,
			FinishesAt:  now,
			CompletedAt: &now,
		},
	}

	searches := map[uint64]map[uint64]*Search{
		1: {
			1: {
				Id:         1,
				StartedAt:  time.Now(),
				FinishesAt: time.Now().Add(time.Second),
			},
			42069: {
				Id:         42069,
				StartedAt:  time.Now(),
				FinishesAt: time.Now().Add(time.Second),
			},
		},
	}

	return &fakeRepository{
		lastStaffId:    2,
		lastSearchId:   1,
		lastTrainingId: 1,
		staff:          staff,
		searches:       searches,
		trainings:      trainings,
	}
}

func (r *fakeRepository) GoOnStrike(ctx context.Context, companyId uint64) error {
	fiveDaysFromNow := time.Now().Add(5 * 24 * time.Hour)
	for _, staff := range r.staff[companyId] {
		staff.OnStrikeUntil = &fiveDaysFromNow
	}
	return nil
}

func (r *fakeRepository) GetSalaries(ctx context.Context, end time.Time) ([]*Payroll, error) {
	payrolls := make([]*Payroll, 0)
	for companyId, staffs := range r.staff {
		total := 0

		for _, staff := range staffs {
			total += int(staff.Salary)
		}

		payrolls = append(payrolls, &Payroll{
			CompanyId: companyId,
			Total:     total,
			Cash:      int(companyId * 10_000_00),
		})
	}
	return payrolls, nil
}

func (r *fakeRepository) PaySalaries(ctx context.Context, total int, companyId uint64) error {
	for _, staff := range r.staff[companyId] {
		staff.OnStrikeUntil = nil
	}
	return nil
}

func (r *fakeRepository) GetSearches(ctx context.Context, companyId uint64) ([]*Search, error) {
	searches := make([]*Search, 0)
	found, ok := r.searches[companyId]
	if ok {
		for _, search := range found {
			searches = append(searches, search)
		}
	}
	return searches, nil
}

func (r *fakeRepository) GetStaff(ctx context.Context, companyId uint64) ([]*Staff, error) {
	staff := make([]*Staff, 0)
	for _, members := range r.staff {
		for _, member := range members {
			if member.Employer == companyId || (member.Poacher != nil && *member.Poacher == companyId) {
				staff = append(staff, member)
			}
		}
	}
	return staff, nil
}

func (r *fakeRepository) GetStaffById(ctx context.Context, staffId uint64) (*Staff, error) {
	for _, staff := range r.staff {
		for _, member := range staff {
			if member.Id == staffId {
				return member, nil
			}
		}
	}

	return nil, ErrStaffNotFound
}

func (r *fakeRepository) RandomStaff(ctx context.Context, companyId uint64) (*Staff, error) {
	for id, staff := range r.staff {
		if id != companyId {
			keys := make([]uint64, 0, len(staff))
			for k := range staff {
				keys = append(keys, k)
			}

			selected := uint64(rand.Intn(len(keys)))
			member := staff[keys[selected]]

			if member.Poacher != nil && *member.Poacher == companyId {
				if len(keys) > 1 {
					current := selected
					for current == selected {
						selected = uint64(rand.Intn(len(keys)))
					}
					member = staff[keys[selected]]
				}
			}

			return member, nil
		}
	}

	return nil, ErrNoStaffFound
}

func (r *fakeRepository) SaveStaff(ctx context.Context, staff *Staff, companyId uint64) (*Staff, error) {
	r.lastStaffId++
	if _, ok := r.staff[companyId]; !ok {
		r.staff[companyId] = make(map[uint64]*Staff)
	}

	staff.Id = r.lastStaffId
	staff.Employer = companyId
	r.staff[companyId][r.lastStaffId] = staff

	return staff, nil
}

func (r *fakeRepository) UpdateStaff(ctx context.Context, staff *Staff) error {
	if members, ok := r.staff[staff.Employer]; ok {
		for _, member := range members {
			if member.Id == staff.Id {
				member = staff
			}
		}
	}
	return nil
}

func (r *fakeRepository) StartSearch(ctx context.Context, expertise int64, finishTime time.Time, companyId uint64) (*Search, error) {
	r.lastSearchId++
	search := &Search{Id: r.lastSearchId, FinishesAt: finishTime, Expertise: expertise}
	r.searches[companyId][search.Id] = search
	return search, nil
}

func (r *fakeRepository) DeleteSearch(ctx context.Context, searchId, companyId uint64) error {
	searches, ok := r.searches[companyId]
	if !ok {
		return ErrSearchNotFound
	}
	delete(searches, searchId)
	return nil
}

func (r *fakeRepository) GetTraining(ctx context.Context, trainingId, companyId uint64) (*Training, error) {
	training, ok := r.trainings[trainingId]
	if !ok {
		return nil, ErrTrainingNotFound
	}
	return training, nil
}

func (r *fakeRepository) SaveTraining(ctx context.Context, training *Training) (*Training, error) {
	r.lastTrainingId++
	training.Id = r.lastTrainingId
	r.trainings[training.Id] = training
	return training, nil
}

func (r *fakeRepository) UpdateTraining(ctx context.Context, training *Training) error {
	r.trainings[training.Id] = training
	return nil
}
