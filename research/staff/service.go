package staff

import (
	"api/notification"
	"api/scheduler"
	"api/server"
	"context"
	"fmt"
	"log"
	"math"
	"math/rand"
	"time"
)

type Status int

const SEARCH_DURATION = 12 * time.Hour
const TRAINING_DURATION = 8 * time.Hour

const (
	PENDING Status = iota
	HIRED
)

const (
	GRADUATE = iota
	EXPERIENCED
)

var (
	NAMES = []string{
		"James", "Robert", "John", "Michael", "David", "William", "Richard",
		"Joseph", "Thomas", "Christopher", "Mary", "Patricia", "Jennifer",
		"Linda", "Elizabeth", "Barbara", "Susan", "Jessica", "Sarah", "Karen",
	}

	LASTNAMES = []string{
		"Smith", "Johnson", "Williams", "Brown", "Jones", "Garcia", "Miller",
		"Davis", "Rodriguez", "Martinez",
	}
)

type (
	OfferExpired struct {
		StaffId   uint64
		CompanyId uint64
	}

	SearchFinished struct {
		CompanyId uint64
		SearchId  uint64
		Expertise int
	}

	TrainingCompleted struct {
		TrainingId uint64
		CompanyId  uint64
	}

	Payroll struct {
		CompanyId uint64 `db:"company_id"`
		Total     int    `db:"total"`
		Cash      int    `db:"available_cash"`
	}

	Search struct {
		Id         uint64    `db:"id" json:"id" goqu:"skipinsert,skipupdate"`
		StartedAt  time.Time `db:"started_at" json:"-"`
		FinishesAt time.Time `db:"finishes_at" json:"finishes_at"`
		CompanyId  uint64    `db:"company_id" json:"-"`
		Expertise  int64     `db:"expertise" json:"expertise"`
	}

	Training struct {
		Id          uint64     `db:"id" goqu:"skipinsert" json:"id"`
		Result      uint8      `db:"result" json:"result,omitempty"`
		Investment  uint64     `db:"investment" json:"-"`
		StaffId     uint64     `db:"staff_id" json:"-"`
		CompanyId   uint64     `db:"company_id" json:"-"`
		StartedAt   time.Time  `db:"started_at" json:"started_at"`
		FinishesAt  time.Time  `db:"finishes_at" json:"finishes_at,omitempty"`
		CompletedAt *time.Time `db:"completed_at" json:"completed_at,omitempty"`
	}

	Staff struct {
		Id            uint64      `db:"id" json:"id"`
		Name          string      `db:"name" json:"name"`
		Skill         uint8       `db:"skill" json:"skill"`
		Talent        uint8       `db:"talent" json:"-"`
		Salary        uint64      `db:"salary" json:"salary"`
		Status        Status      `db:"status" json:"status"`
		Offer         uint64      `db:"offer" json:"offer"`
		Poacher       *uint64     `db:"poacher_id" json:"-"`
		Employer      uint64      `db:"company_id" json:"-"`
		TrainingUntil *time.Time  `db:"training_until" json:"training_until" goqu:"skipinsert,skipupdate"`
		BusyUntil     *time.Time  `db:"busy_until" json:"busy_until" goqu:"skipinsert,skipupdate"`
		Trainings     []*Training `json:"trainings"`
		HiredAt       time.Time   `db:"hired_at" json:"hired_at"`
		OnStrikeUntil *time.Time  `db:"on_strike_until" json:"on_strike_until" goqu:"skipinsert,skipupdate"`
	}

	Service interface {
		FindGraduate(ctx context.Context, companyId uint64) (*Search, error)
		FindExperienced(ctx context.Context, companyId uint64) (*Search, error)
		CancelSearch(ctx context.Context, searchId, companyId uint64) error

		GetGraduate(ctx context.Context, companyId uint64) (*Staff, error)
		GetExperienced(ctx context.Context, companyId uint64) (*Staff, error)
		GetSearches(ctx context.Context, companyId uint64) ([]*Search, error)

		GetStaff(ctx context.Context, companyId uint64) ([]*Staff, error)
		GetStaffById(ctx context.Context, staffId, companyId uint64) (*Staff, error)
		HireStaff(ctx context.Context, staffId, companyId uint64) (*Staff, error)
		MakeOffer(ctx context.Context, offer, staffId, companyId uint64) (*Staff, error)

		IncreaseSalary(ctx context.Context, salary, staffId, companyId uint64) (*Staff, error)
		PaySalaries(ctx context.Context, end time.Time) error
		Train(ctx context.Context, staffId, companyId uint64) (*Training, error)
		FinishTraining(ctx context.Context, trainingId, companyId uint64) error

		HandleOfferExpired(payload OfferExpired) error
		HandleSearchFinished(payload SearchFinished) error
		HandleTrainingCompleted(payload TrainingCompleted) error
	}

	service struct {
		repository Repository
		timer      *scheduler.Scheduler
		notifier   notification.Notifier
		logger     *log.Logger
	}
)

func NewService(
	repository Repository,
	timer *scheduler.Scheduler,
	notifier notification.Notifier,
	logger *log.Logger,
) Service {
	return &service{repository, timer, notifier, logger}
}

func (s *service) HandleTrainingCompleted(payload TrainingCompleted) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	return s.FinishTraining(ctx, payload.TrainingId, payload.CompanyId)
}

func (s *service) HandleOfferExpired(payload OfferExpired) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	_, err := s.HireStaff(ctx, payload.StaffId, payload.CompanyId)
	return err

}

func (s *service) HandleSearchFinished(payload SearchFinished) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	if err := s.repository.DeleteSearch(ctx, payload.SearchId, payload.CompanyId); err != nil {
		return err
	}

	var err error
	var candidate *Staff

	if payload.Expertise == GRADUATE {
		candidate, err = s.GetGraduate(ctx, payload.CompanyId)
	} else if payload.Expertise == EXPERIENCED {
		candidate, err = s.GetExperienced(ctx, payload.CompanyId)
	}

	if err != nil {
		return err
	}

	message := fmt.Sprintf("%s is available for hire", candidate.Name)
	if err := s.notifier.Notify(ctx, message, int64(payload.CompanyId)); err != nil {
		s.logger.Printf("Error notifying graduate available for hire: %s\n", err)
	}

	return err
}

func (s *service) GetStaff(ctx context.Context, companyId uint64) ([]*Staff, error) {
	return s.repository.GetStaff(ctx, companyId)
}

func (s *service) GetStaffById(ctx context.Context, staffId, companyId uint64) (*Staff, error) {
	staff, err := s.repository.GetStaffById(ctx, staffId)
	if err != nil {
		return nil, err
	}

	if staff.Employer != companyId {
		return nil, ErrStaffNotFound
	}

	return staff, nil
}

func (s *service) GetSearches(ctx context.Context, companyId uint64) ([]*Search, error) {
	return s.repository.GetSearches(ctx, companyId)
}

func (s *service) FindGraduate(ctx context.Context, companyId uint64) (*Search, error) {
	finishTime := time.Now().Add(SEARCH_DURATION)
	search, err := s.repository.StartSearch(ctx, GRADUATE, finishTime, companyId)
	if err != nil {
		return nil, err
	}

	s.timer.Schedule(fmt.Sprintf("SEARCH_%d", search.Id), finishTime, SearchFinished{
		SearchId:  search.Id,
		CompanyId: companyId,
		Expertise: GRADUATE,
	})

	return search, nil
}

func (s *service) GetGraduate(ctx context.Context, companyId uint64) (*Staff, error) {
	randomizer := rand.New(rand.NewSource(time.Now().UnixNano()))

	skill := randomizer.Intn(100)
	talent := randomizer.Intn(100)
	salary := randomizer.Intn(1_000_00) + 1_000_00

	name := NAMES[randomizer.Intn(len(NAMES))] + " " + LASTNAMES[randomizer.Intn(len(LASTNAMES))]

	staff := &Staff{
		Name:   name,
		Skill:  uint8(skill),
		Talent: uint8(talent),
		Salary: uint64(salary),
		Status: PENDING,
	}

	return s.repository.SaveStaff(ctx, staff, companyId)
}

func (s *service) FindExperienced(ctx context.Context, companyId uint64) (*Search, error) {
	finishTime := time.Now().Add(SEARCH_DURATION)
	search, err := s.repository.StartSearch(ctx, EXPERIENCED, finishTime, companyId)
	if err != nil {
		return nil, err
	}

	s.timer.Schedule(fmt.Sprintf("SEARCH_%d", search.Id), finishTime, SearchFinished{
		SearchId:  search.Id,
		CompanyId: companyId,
		Expertise: EXPERIENCED,
	})

	return search, nil
}

func (s *service) GetExperienced(ctx context.Context, companyId uint64) (*Staff, error) {
	staff, err := s.repository.RandomStaff(ctx, companyId)
	if err != nil {
		return nil, err
	}

	staff.Poacher = &companyId
	err = s.repository.UpdateStaff(ctx, staff)

	return staff, err
}

func (s *service) CancelSearch(ctx context.Context, searchId, companyId uint64) error {
	err := s.repository.DeleteSearch(ctx, searchId, companyId)
	if err != nil {
		return err
	}

	s.timer.Remove(fmt.Sprintf("SEARCH_%d", searchId))
	return nil
}

func (s *service) HireStaff(ctx context.Context, staffId, companyId uint64) (*Staff, error) {
	staff, err := s.repository.GetStaffById(ctx, staffId)
	if err != nil {
		return nil, err
	}

	currentEmployer := staff.Employer

	if (staff.Poacher != nil && *staff.Poacher != companyId) || (staff.Poacher == nil && staff.Employer != companyId) {
		return nil, ErrStaffNotFound
	}

	// If hiring experienced, update the salary and clean up poaching fields
	if staff.Poacher != nil {
		staff.Salary = staff.Offer
		staff.Employer = *staff.Poacher

		staff.Offer = 0
		staff.Poacher = nil
	}

	staff.Status = HIRED
	if err := s.repository.UpdateStaff(ctx, staff); err != nil {
		return nil, err
	}

	message := fmt.Sprintf("%s accepted your offer", staff.Name)
	if err := s.notifier.Notify(ctx, message, int64(companyId)); err != nil {
		s.logger.Printf("Error notifying poacher of offer accepted: %s\n", err)
	}

	message = fmt.Sprintf("%s accepted the offer and no longer works for us", staff.Name)
	if err := s.notifier.Notify(ctx, message, int64(currentEmployer)); err != nil {
		s.logger.Printf("Error notifying employer of offer accepted: %s\n", err)
	}

	return staff, nil
}

func (s *service) MakeOffer(ctx context.Context, offer, staffId, companyId uint64) (*Staff, error) {
	// Must save the offer and notify the current employer of the staff
	staff, err := s.repository.GetStaffById(ctx, staffId)
	if err != nil {
		return nil, err
	}

	if staff.Poacher == nil || *staff.Poacher != companyId {
		return nil, ErrStaffNotFound
	}

	if offer <= staff.Salary {
		return nil, server.NewBusinessRuleError("offer is too low")
	}

	staff.Offer = offer
	if err := s.repository.UpdateStaff(ctx, staff); err != nil {
		return nil, err
	}

	message := fmt.Sprintf("%s received an offer of $ %.2f", staff.Name, float64(staff.Offer)/100)
	if err := s.notifier.Notify(ctx, message, int64(companyId)); err != nil {
		s.logger.Printf("Error notifying offer: %s\n", err)
	}

	s.timer.Schedule(fmt.Sprintf("OFFER_%d", staffId), time.Now().Add(48*time.Hour), OfferExpired{
		StaffId:   staffId,
		CompanyId: companyId,
	})

	return staff, nil
}

func (s *service) IncreaseSalary(ctx context.Context, salary, staffId, companyId uint64) (*Staff, error) {
	staff, err := s.repository.GetStaffById(ctx, staffId)
	if err != nil {
		return nil, err
	}

	if staff.Employer != companyId {
		return nil, ErrStaffNotFound
	}

	if salary <= staff.Salary {
		return nil, server.NewBusinessRuleError("new salary must be higher than current salary")
	}

	var hasPoacher bool
	if staff.Poacher != nil {
		hasPoacher = true
		if salary <= staff.Offer {
			return nil, server.NewBusinessRuleError("new salary must be higher than current offer")
		}
		staff.Offer = 0
		staff.Poacher = nil
	}

	staff.Salary = salary
	if err := s.repository.UpdateStaff(ctx, staff); err != nil {
		return nil, err
	}

	if hasPoacher {
		message := fmt.Sprintf("%s has declined your offer", staff.Name)
		if err := s.notifier.Notify(ctx, message, int64(companyId)); err != nil {
			s.logger.Printf("Error notifying offer declined: %s\n", err)
		}
	}

	return staff, nil
}

func (s *service) Train(ctx context.Context, staffId, companyId uint64) (*Training, error) {
	staff, err := s.repository.GetStaffById(ctx, staffId)
	if err != nil {
		return nil, err
	}

	if staff.Employer != companyId {
		return nil, ErrStaffNotFound
	}

	// Calculate time (relative to skill)
	trainingDuration, err := time.ParseDuration(fmt.Sprintf("%dh", int64(staff.Skill)/10))
	if err != nil {
		return nil, err
	}

	duration := TRAINING_DURATION + trainingDuration
	finishesAt := time.Now().Add(duration)

	// Save training
	training, err := s.repository.SaveTraining(ctx, &Training{
		StaffId:    staffId,
		CompanyId:  companyId,
		FinishesAt: finishesAt,
		Investment: 1000000 + (1000000 * (uint64(staff.Skill) / 10)),
	})

	if err != nil {
		return nil, err
	}

	s.timer.Schedule(fmt.Sprintf("TRAINING_%d", training.Id), finishesAt, TrainingCompleted{
		TrainingId: training.Id,
		CompanyId:  companyId,
	})

	return training, nil
}

func (s *service) FinishTraining(ctx context.Context, trainingId, companyId uint64) error {
	training, err := s.repository.GetTraining(ctx, trainingId, companyId)
	if err != nil {
		return err
	}

	staff, err := s.repository.GetStaffById(ctx, training.StaffId)
	if err != nil {
		return err
	}

	// Complete training
	now := time.Now()
	training.CompletedAt = &now

	// Calculate points (relative to talent, e.g., rand(0, talent / 10))
	randomizer := rand.New(rand.NewSource(time.Now().UnixNano()))
	base := int(math.Min(float64(staff.Talent), float64(staff.Talent/10)))
	training.Result = uint8(randomizer.Intn(base) + (base / 2))

	// Save new skill
	return s.repository.UpdateTraining(ctx, training)
}

func (s *service) PaySalaries(ctx context.Context, end time.Time) error {
	salaries, err := s.repository.GetSalaries(ctx, end)
	if err != nil {
		return err
	}

	for _, salary := range salaries {
		if salary.Cash < salary.Total {
			if err := s.repository.GoOnStrike(ctx, salary.CompanyId); err != nil {
				s.logger.Printf("Error saving strike: %s", err)
			}
		} else {
			if err := s.repository.PaySalaries(ctx, salary.Total, salary.CompanyId); err != nil {
				s.logger.Printf("Error paying salaries: %s", err)
			}
		}
	}

	return nil
}
