package staff_test

import (
	"api/accounting"
	"api/company"
	"api/database"
	"api/research/staff"
	"context"
	"testing"
	"time"
)

func TestResearchRepository(t *testing.T) {
	conn, err := database.GetConnection(database.SQLITE, "../../test.db")
	if err != nil {
		t.Fatalf("could not connect to database: %s", err)
	}

	tx, err := conn.DB.Begin()
	if err != nil {
		t.Fatalf("could not start transaction: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO companies (id, name, email, password)
        VALUES (1, "Test", "test", "test"), (2, "Other", "other", "other")
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO research_staff (id, name, salary, company_id, poacher_id, skill, hired_at) VALUES
        (1, "Test", 200000, 1, null, 0, '2010-10-10 10:10:10'),
        (2, "Other", 100000, 2, 1, 90, '2012-10-10 10:10:10'),
        (3, "T", 500000, 2, null, 0, '2014-10-10 10:10:10'),
        (4, "B", 500000, 2, null, 10, '2020-10-10 10:10:10')
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO researches (id, company_id, resource_id, finishes_at, completed_at) VALUES
        (1, 1, 1, '2150-12-13 13:53:55', NULL),
        (2, 2, 1, '2015-12-13 13:53:55', '2015-12-13 13:53:56'),
        (3, 1, 1, '2015-12-13 13:53:55', '2015-12-13 13:53:56'),
        (4, 2, 1, '2150-12-13 13:53:55', NULL),
        (5, 1, 1, '2151-11-23 23:53:55', NULL),
        (6, 2, 1, '2250-02-03 13:53:55', NULL)
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO assigned_staff (research_id, staff_id)
        VALUES (1, 1), (2, 2), (3, 1), (4, 3), (5, 1), (6, 3);
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO trainings (id, staff_id, company_id, investment, result, finishes_at, completed_at) VALUES
        (1, 2, 1, 10000000, 52, '2023-03-01 01:09:00', '2023-03-03 03:03:03'),
        (2, 3, 2, 10000000, 52, '2123-01-01 10:09:40', NULL),
        (3, 3, 2, 10000000, 52, '2125-03-01 11:29:00', NULL),
        (4, 4, 1, 10000000, 12, '2125-05-11 11:29:00', NULL)
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO transactions (company_id, value)
        VALUES (1, 1000000);
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO staff_searches (id, company_id, finishes_at, expertise) VALUES
        (1, 1, '2023-12-25 22:23:32', 0),
        (2, 1, '2034-12-31 22:15:54', 0),
        (3, 1, '2034-02-27 02:25:55', 1),
        (4, 2, '2020-12-12 12:12:12', 0),
        (5, 2, '2130-02-27 21:16:21', 1)
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if err := tx.Commit(); err != nil {
		t.Fatalf("could not commit transaction: %s", err)
	}

	t.Cleanup(func() {
		if _, err := conn.DB.Exec(`DELETE FROM staff_searches`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM transactions`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM trainings`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM companies`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM researches`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM assigned_staff`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM research_staff`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
	})

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	accountingRepo := accounting.NewRepository(conn)
	repository := staff.NewRepository(conn, accountingRepo)
	companyRepo := company.NewRepository(conn, accountingRepo)

	t.Run("GetSalaries", func(t *testing.T) {
		t.Run("filter", func(t *testing.T) {
			period := time.Date(2014, 3, 4, 21, 45, 30, 0, time.UTC)
			salaries, err := repository.GetSalaries(ctx, period)
			if err != nil {
				t.Fatalf("could not get salaries: %s", err)
			}

			if len(salaries) != 2 {
				t.Fatalf("expected %d entry, got %d", 2, len(salaries))
			}

			for _, salary := range salaries {
				if salary.CompanyId == 1 {
					if salary.Total != 200000 {
						t.Errorf("expected total %d, got %d", 200000, salary.Total)
					}
					if salary.Cash != 1000000 {
						t.Errorf("expected cash %d, got %d", 1000000, salary.Cash)
					}
				}

				if salary.CompanyId == 2 {
					if salary.Total != 100000 {
						t.Errorf("expected total %d, got %d", 100000, salary.Total)
					}
					if salary.Cash != 0 {
						t.Errorf("expected no cash, got %d", salary.Cash)
					}
				}
			}
		})

		t.Run("everyone", func(t *testing.T) {
			period := time.Date(2024, 3, 4, 21, 45, 30, 0, time.UTC)
			salaries, err := repository.GetSalaries(ctx, period)

			if err != nil {
				t.Fatalf("could not get salaries: %s", err)
			}

			if len(salaries) != 2 {
				t.Errorf("expected %d entries, got %d", 2, len(salaries))
			}

			for _, salary := range salaries {
				if salary.CompanyId == 1 {
					if salary.Total != 200000 {
						t.Errorf("expected total %d, got %d", 200000, salary.Total)
					}
					if salary.Cash != 1000000 {
						t.Errorf("expected cash %d, got %d", 1000000, salary.Cash)
					}
				}

				if salary.CompanyId == 2 {
					if salary.Total != 1100000 {
						t.Errorf("expected total %d, got %d", 1100000, salary.Total)
					}
					if salary.Cash != 0 {
						t.Errorf("expected no cash, got %d", salary.Cash)
					}
				}
			}
		})
	})

	t.Run("GoOnStrike", func(t *testing.T) {
		if err := repository.GoOnStrike(ctx, 2); err != nil {
			t.Fatalf("could not save strike: %s", err)
		}

		staff, err := repository.GetStaff(ctx, 2)
		if err != nil {
			t.Fatalf("could not get staff: %s", err)
		}

		for _, member := range staff {
			if member.OnStrikeUntil == nil {
				t.Errorf("staff %d should be on strike", member.Id)
			}
		}
	})

	t.Run("PaySalaries", func(t *testing.T) {
		t.Run("reduces cash", func(t *testing.T) {
			if err := repository.PaySalaries(ctx, 1, 1); err != nil {
				t.Fatalf("could not pay salaries: %s", err)
			}

			company, err := companyRepo.GetById(ctx, 1)
			if err != nil {
				t.Fatalf("could not get company: %s", err)
			}

			if company.AvailableCash != 999999 {
				t.Errorf("expected cash %d, got %d", 999999, company.AvailableCash)
			}
		})

		t.Run("resets strike", func(t *testing.T) {
			if err := repository.PaySalaries(ctx, 0, 2); err != nil {
				t.Fatalf("could not pay salaries: %s", err)
			}

			staff, err := repository.GetStaff(ctx, 2)
			if err != nil {
				t.Fatalf("could not get staff: %s", err)
			}

			for _, member := range staff {
				if member.OnStrikeUntil != nil {
					t.Errorf("staff %d should not be on strike", member.Id)
				}
			}
		})
	})

	t.Run("GetStaff", func(t *testing.T) {
		t.Run("brings poached as well", func(t *testing.T) {
			staff, err := repository.GetStaff(ctx, 1)
			if err != nil {
				t.Fatalf("could not get staff: %s", err)
			}

			if len(staff) != 2 {
				t.Errorf("expected %d staff, got %d", 2, len(staff))
			}
		})

		t.Run("training until", func(t *testing.T) {
			t.Run("should ignore old trainings", func(t *testing.T) {
				staff, err := repository.GetStaff(ctx, 1)
				if err != nil {
					t.Fatalf("could not get staff: %s", err)
				}

				if len(staff) != 2 {
					t.Errorf("expected %d staff, got %d", 2, len(staff))
				}

				for _, member := range staff {
					if member.Id == 1 && member.TrainingUntil != nil {
						t.Errorf("expected no busy until, got %s", *member.TrainingUntil)
					}
					if member.Id == 2 && member.TrainingUntil != nil {
						t.Errorf("expected no busy until, got %s", *member.TrainingUntil)
					}
				}
			})

			t.Run("should consider only one training", func(t *testing.T) {
				staff, err := repository.GetStaff(ctx, 2)
				if err != nil {
					t.Fatalf("could not get staff: %s", err)
				}

				if len(staff) != 3 {
					t.Fatalf("expected %d staff, got %d", 3, len(staff))
				}

				for _, member := range staff {
					if member.Id == 2 && member.TrainingUntil != nil {
						t.Errorf("expected no busy until, got %s", *member.TrainingUntil)
					}
					if member.Id == 3 {
						if member.TrainingUntil == nil {
							t.Fatal("expected busy until")
						}
						year, month, day := member.TrainingUntil.Date()
						if year != 2125 {
							t.Errorf("expected year %d, got %d", 2125, year)
						}
						if month != 3 {
							t.Errorf("expected month %d, got %d", 3, month)
						}
						if day != 1 {
							t.Errorf("expected day %d, got %d", 1, day)
						}
					}
					if member.Id == 4 {
						if member.TrainingUntil == nil {
							t.Error("expected busy until")
						}
						year, month, day := member.TrainingUntil.Date()
						if year != 2125 {
							t.Errorf("expected year %d, got %d", 2125, year)
						}
						if month != 5 {
							t.Errorf("expected month %d, got %d", 5, month)
						}
						if day != 11 {
							t.Errorf("expected day %d, got %d", 11, day)
						}
					}
				}
			})
		})

		t.Run("should include completed trainings", func(t *testing.T) {
			staff, err := repository.GetStaff(ctx, 2)
			if err != nil {
				t.Fatalf("could not get staff: %s", err)
			}

			if len(staff) != 3 {
				t.Fatalf("expected %d staff, got %d", 3, len(staff))
			}

			for _, member := range staff {
				if member.Id == 2 && len(member.Trainings) != 1 {
					t.Errorf("expected %d training, got %d", 1, len(member.Trainings))
				}
				if member.Id == 3 && len(member.Trainings) != 0 {
					t.Errorf("expected no trainings, got %d", len(member.Trainings))
				}
				if member.Id == 4 && len(member.Trainings) != 0 {
					t.Errorf("expected no trainings, got %d", len(member.Trainings))
				}
			}
		})

		t.Run("busy_until", func(t *testing.T) {
			staff, err := repository.GetStaff(ctx, 1)
			if err != nil {
				t.Fatalf("could not get staff: %s", err)
			}

			if len(staff) != 2 {
				t.Fatalf("expected %d staff, got %d", 2, len(staff))
			}

			for _, member := range staff {
				if member.Id == 1 {
					if member.BusyUntil == nil {
						t.Errorf("staff %d should be busy", member.Id)
					} else {
						year, month, day := member.BusyUntil.Date()
						if year != 2151 {
							t.Errorf("expected year %d, got %d", 2150, year)
						}
						if month != 11 {
							t.Errorf("expected month %d, got %d", 11, month)
						}
						if day != 23 {
							t.Errorf("expected day %d, got %d", 23, day)
						}
					}
				}
				if member.Id == 2 && member.BusyUntil != nil {
					t.Errorf("staff %d should not be busy", member.Id)
				}
			}

			staff, err = repository.GetStaff(ctx, 2)
			if err != nil {
				t.Fatalf("could not get staff: %s", err)
			}

			if len(staff) != 3 {
				t.Fatalf("expected %d staff, got %d", 3, len(staff))
			}

			for _, member := range staff {
				if member.Id == 2 && member.BusyUntil != nil {
					t.Errorf("staff %d should not be busy", member.Id)
				}
				if member.Id == 3 {
					if member.BusyUntil == nil {
						t.Errorf("staff %d should be busy", member.Id)
					} else {
						year, month, day := member.BusyUntil.Date()
						if year != 2250 {
							t.Errorf("expected year %d, got %d", 2250, year)
						}
						if month != 2 {
							t.Errorf("expected month %d, got %d", 2, month)
						}
						if day != 3 {
							t.Errorf("expected day %d, got %d", 3, day)
						}
					}
				}
				if member.Id == 4 && member.BusyUntil != nil {
					t.Errorf("staff %d should not be busy", member.Id)
				}
			}
		})
	})

	t.Run("GetStaffById", func(t *testing.T) {
		t.Run("not found", func(t *testing.T) {
			_, err := repository.GetStaffById(ctx, 1543)
			if err != staff.ErrStaffNotFound {
				t.Errorf("expected error \"%s\", got \"%s\"", staff.ErrStaffNotFound, err)
			}
		})

		t.Run("found", func(t *testing.T) {
			staff, err := repository.GetStaffById(ctx, 3)
			if err != nil {
				t.Fatalf("could not find staff: %s", err)
			}

			if staff.Id != 3 {
				t.Errorf("expected id %d, got %d", 3, staff.Id)
			}
		})

		t.Run("should ignore old trainings", func(t *testing.T) {
			staff, err := repository.GetStaffById(ctx, 2)
			if err != nil {
				t.Fatalf("could not get staff by id: %s", err)
			}

			if staff.TrainingUntil != nil {
				t.Errorf("should not be busy, got %s", staff.TrainingUntil.String())
			}
		})

		t.Run("should consider only one training", func(t *testing.T) {
			staff, err := repository.GetStaffById(ctx, 3)
			if err != nil {
				t.Fatalf("could not get staff by id: %s", err)
			}

			if staff.TrainingUntil == nil {
				t.Fatal("should be busy")
			}

			year, month, day := staff.TrainingUntil.Date()
			if year != 2125 {
				t.Errorf("expected year %d, got %d", 2125, year)
			}
			if month != 3 {
				t.Errorf("expected month %d, got %d", 3, month)
			}
			if day != 1 {
				t.Errorf("expected day %d, got %d", 1, day)
			}
		})
	})

	t.Run("RandomStaff", func(t *testing.T) {
		t.Run("should not bring currently poached", func(t *testing.T) {
			staff, err := repository.RandomStaff(ctx, 1)
			if err != nil {
				t.Fatalf("could not get random staff: %s", err)
			}

			if staff.Id != 3 {
				t.Errorf("expected id %d, got %d", 3, staff.Id)
			}
		})
	})

	t.Run("GetSearches", func(t *testing.T) {
		searches, err := repository.GetSearches(ctx, 1)
		if err != nil {
			t.Fatalf("could not get searches: %s", err)
		}

		if len(searches) != 2 {
			t.Errorf("expected %d searches, got %d", 2, len(searches))
		}

		for _, search := range searches {
			if search.Id != 2 && search.Id != 3 {
				t.Errorf("expected searches 2 and 3, got %d", search.Id)
			}
			if search.Id == 2 && search.Expertise != staff.GRADUATE {
				t.Errorf("expected expertise %d, got %d", staff.GRADUATE, search.Expertise)
			}
			if search.Id == 3 && search.Expertise != staff.EXPERIENCED {
				t.Errorf("expected expertise %d, got %d", staff.EXPERIENCED, search.Expertise)
			}
		}

		searches, err = repository.GetSearches(ctx, 2)
		if err != nil {
			t.Fatalf("could not get searches: %s", err)
		}

		if len(searches) != 1 {
			t.Errorf("expected %d searches, got %d", 1, len(searches))
		}

		if searches[0].Id != 5 {
			t.Errorf("expected searches 5, got %d", searches[0].Id)
		}

		if searches[0].Expertise != staff.EXPERIENCED {
			t.Errorf("expected expertise %d, got %d", staff.EXPERIENCED, searches[0].Expertise)
		}
	})

	t.Run("SaveTraining", func(t *testing.T) {
		t.Run("should save a transaction", func(t *testing.T) {
			training, err := repository.SaveTraining(ctx, &staff.Training{
				Investment: 500000,
				StaffId:    1,
				CompanyId:  1,
				FinishesAt: time.Now().Add(time.Second),
			})

			if err != nil {
				t.Fatalf("could not save training: %s", err)
			}

			if training.Id == 0 {
				t.Error("should have set an id")
			}

			company, err := companyRepo.GetById(ctx, 1)
			if err != nil {
				t.Fatalf("could not get company: %s", err)
			}

			expectedCash := 499999
			if company.AvailableCash != expectedCash {
				t.Errorf("expected cash %d, got %d", expectedCash, company.AvailableCash)
			}
		})
	})

	t.Run("UpdateTraining", func(t *testing.T) {
		t.Run("update staff skill", func(t *testing.T) {
			now := time.Now()

			err := repository.UpdateTraining(ctx, &staff.Training{
				Id:          1,
				Investment:  10000000,
				StaffId:     2,
				CompanyId:   1,
				Result:      15,
				CompletedAt: &now,
			})

			if err != nil {
				t.Fatalf("could not update training: %s", err)
			}

			training, err := repository.GetTraining(ctx, 1, 1)
			if err != nil {
				t.Fatalf("could not get training: %s", err)
			}

			if training.CompletedAt == nil {
				t.Error("should have updated completed_at")
			}

			if training.Result == 0 {
				t.Error("should have updated result")
			}

			staff, err := repository.GetStaffById(ctx, 2)
			if err != nil {
				t.Fatalf("could not get staff: %s", err)
			}

			if staff.Skill != 100 {
				t.Errorf("expected skill %d, got %d", 100, staff.Skill)
			}
		})
	})

	t.Run("DeleteSearch", func(t *testing.T) {
		t.Run("should return error if no rows affected", func(t *testing.T) {
			err := repository.DeleteSearch(ctx, 23, 3)
			if err != staff.ErrSearchNotFound {
				t.Errorf("expected error \"%s\", got \"%s\"", staff.ErrSearchNotFound, err)
			}
		})

		t.Run("should delete normally", func(t *testing.T) {
			search, err := repository.StartSearch(ctx, staff.GRADUATE, time.Now().Add(time.Second), 1)
			if err != nil {
				t.Fatalf("could not start search: %s", err)
			}

			if err := repository.DeleteSearch(ctx, search.Id, 1); err != nil {
				t.Fatalf("could not delete search: %s", err)
			}
		})
	})
}
