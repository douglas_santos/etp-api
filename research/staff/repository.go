package staff

import (
	"api/accounting"
	"api/database"
	"context"
	"errors"
	"time"

	"github.com/doug-martin/goqu/v9"
)

var (
	ErrNoStaffFound     = errors.New("no staff found")
	ErrStaffNotFound    = errors.New("staff not found")
	ErrTrainingNotFound = errors.New("training not found")
	ErrSearchNotFound   = errors.New("search not found")
)

type (
	Repository interface {
		GetStaff(ctx context.Context, companyId uint64) ([]*Staff, error)
		GetStaffById(ctx context.Context, staffId uint64) (*Staff, error)
		RandomStaff(ctx context.Context, companyId uint64) (*Staff, error)

		GetSearches(ctx context.Context, companyId uint64) ([]*Search, error)
		StartSearch(ctx context.Context, expertise int64, finishTime time.Time, companyId uint64) (*Search, error)
		DeleteSearch(ctx context.Context, searchId, companyId uint64) error

		SaveStaff(ctx context.Context, staff *Staff, companyId uint64) (*Staff, error)
		UpdateStaff(ctx context.Context, staff *Staff) error

		GetSalaries(ctx context.Context, end time.Time) ([]*Payroll, error)
		PaySalaries(ctx context.Context, total int, companyId uint64) error
		GoOnStrike(ctx context.Context, companyId uint64) error

		GetTraining(ctx context.Context, trainingId, companyId uint64) (*Training, error)
		SaveTraining(ctx context.Context, training *Training) (*Training, error)
		UpdateTraining(ctx context.Context, training *Training) error
	}

	goquRepository struct {
		builder        *goqu.Database
		accountingRepo accounting.Repository
	}
)

func NewRepository(conn *database.Connection, accountingRepo accounting.Repository) Repository {
	builder := goqu.New(conn.Driver, conn.DB)
	return &goquRepository{builder, accountingRepo}
}

func (r *goquRepository) GetSearches(ctx context.Context, companyId uint64) ([]*Search, error) {
	searches := make([]*Search, 0)

	err := r.builder.
		Select(goqu.Star()).
		From(goqu.T("staff_searches")).
		Where(goqu.And(
			goqu.I("company_id").Eq(companyId),
			goqu.I("finishes_at").Gt(time.Now()),
		)).
		ScanStructsContext(ctx, &searches)

	if err != nil {
		return nil, err
	}

	return searches, nil
}

func (r *goquRepository) GetStaff(ctx context.Context, companyId uint64) ([]*Staff, error) {
	staff := make([]*Staff, 0)

	err := r.staffSelectDataset().
		Where(goqu.Or(
			goqu.I("rs.company_id").Eq(companyId),
			goqu.I("rs.poacher_id").Eq(companyId),
		)).
		ScanStructsContext(ctx, &staff)

	if err != nil {
		return nil, err
	}

	for _, member := range staff {
		trainings, err := r.getStaffTrainings(ctx, member.Id)
		if err != nil {
			return nil, err
		}

		member.Trainings = trainings
	}

	return staff, nil
}

func (r *goquRepository) GetStaffById(ctx context.Context, staffId uint64) (*Staff, error) {
	staff := new(Staff)

	found, err := r.staffSelectDataset().
		Where(goqu.I("rs.id").Eq(staffId)).
		ScanStructContext(ctx, staff)

	if err != nil {
		return nil, err
	}

	if !found {
		return nil, ErrStaffNotFound
	}

	trainings, err := r.getStaffTrainings(ctx, staff.Id)
	if err != nil {
		return nil, err
	}

	staff.Trainings = trainings

	return staff, nil
}

func (r *goquRepository) staffSelectDataset() *goqu.SelectDataset {
	return r.builder.
		Select(
			goqu.I("rs.id"),
			goqu.I("rs.name"),
			goqu.I("rs.salary"),
			goqu.I("rs.skill"),
			goqu.I("rs.talent"),
			goqu.I("rs.status"),
			goqu.I("rs.offer"),
			goqu.I("rs.company_id"),
			goqu.I("rs.poacher_id"),
			goqu.I("rs.hired_at"),
			goqu.I("rs.on_strike_until"),
			goqu.
				Select(goqu.I("finishes_at")).
				From("trainings").
				Where(goqu.And(
					goqu.I("completed_at").IsNull(),
					goqu.I("finishes_at").Gt(time.Now()),
					goqu.I("rs.id").Eq(goqu.I("staff_id")),
				)).
				Order(goqu.I("finishes_at").Desc()).
				Limit(1).
				As("training_until"),
			goqu.
				Select(goqu.I("r.finishes_at")).
				From(goqu.T("researches").As("r")).
				LeftJoin(
					goqu.T("assigned_staff").As("as"),
					goqu.On(goqu.I("as.research_id").Eq(goqu.I("r.id"))),
				).
				Where(goqu.And(
					goqu.I("r.completed_at").IsNull(),
					goqu.I("r.finishes_at").Gt(time.Now()),
					goqu.I("as.staff_id").Eq(goqu.I("rs.id")),
				)).
				Order(goqu.I("r.finishes_at").Desc()).
				Limit(1).
				As("busy_until"),
		).
		From(goqu.T("research_staff").As("rs"))
}

func (r *goquRepository) getStaffTrainings(ctx context.Context, staffId uint64) ([]*Training, error) {
	trainings := make([]*Training, 0)

	err := r.builder.
		Select(goqu.Star()).
		From(goqu.T("trainings")).
		Where(goqu.And(
			goqu.I("staff_id").Eq(staffId),
			goqu.I("completed_at").IsNotNull(),
			goqu.I("completed_at").Lte(time.Now()),
		)).
		ScanStructsContext(ctx, &trainings)

	if err != nil {
		return nil, err
	}

	return trainings, nil
}

func (r *goquRepository) StartSearch(ctx context.Context, expertise int64, finishTime time.Time, companyId uint64) (*Search, error) {
	search := &Search{
		Expertise:  expertise,
		StartedAt:  time.Now(),
		FinishesAt: finishTime,
		CompanyId:  companyId,
	}

	result, err := r.builder.
		Insert(goqu.T("staff_searches")).
		Rows(search).
		Executor().
		ExecContext(ctx)

	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	search.Id = uint64(id)
	return search, nil
}

func (r *goquRepository) DeleteSearch(ctx context.Context, searchId, companyId uint64) error {
	result, err := r.builder.
		Delete(goqu.T("staff_searches")).
		Where(goqu.And(
			goqu.I("id").Eq(searchId),
			goqu.I("company_id").Eq(companyId),
		)).
		Executor().
		ExecContext(ctx)

	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrSearchNotFound
	}

	return nil
}

func (r *goquRepository) RandomStaff(ctx context.Context, companyId uint64) (*Staff, error) {
	staff := new(Staff)

	found, err := r.builder.
		Select(
			goqu.I("id"),
			goqu.I("name"),
			goqu.I("salary"),
			goqu.I("skill"),
			goqu.I("talent"),
			goqu.I("status"),
			goqu.I("offer"),
			goqu.I("company_id"),
			goqu.I("poacher_id"),
		).
		From(goqu.T("research_staff")).
		Where(goqu.And(
			goqu.I("poacher_id").IsNull(),
			goqu.I("company_id").Neq(companyId),
		)).
		ScanStructContext(ctx, staff)

	if err != nil {
		return nil, err
	}

	if !found {
		return nil, ErrNoStaffFound
	}

	return staff, nil
}

func (r *goquRepository) SaveStaff(ctx context.Context, staff *Staff, companyId uint64) (*Staff, error) {
	result, err := r.builder.
		Insert(goqu.T("research_staff")).
		Rows(goqu.Record{
			"name":       staff.Name,
			"status":     staff.Status,
			"salary":     staff.Salary,
			"skill":      staff.Skill,
			"talent":     staff.Talent,
			"company_id": companyId,
		}).
		Executor().
		ExecContext(ctx)

	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	staff.Id = uint64(id)
	return staff, nil
}

func (r *goquRepository) UpdateStaff(ctx context.Context, staff *Staff) error {
	_, err := r.builder.
		Update(goqu.T("research_staff")).
		Set(goqu.Record{
			"status":     staff.Status,
			"salary":     staff.Salary,
			"offer":      staff.Offer,
			"poacher_id": staff.Poacher,
			"company_id": staff.Employer,
		}).
		Where(goqu.I("id").Eq(staff.Id)).
		Executor().
		ExecContext(ctx)

	if err != nil {
		return err
	}

	return err
}

func (r *goquRepository) GetTraining(ctx context.Context, trainingId, companyId uint64) (*Training, error) {
	training := new(Training)

	found, err := r.builder.
		Select(goqu.Star()).
		From(goqu.T("trainings")).
		Where(goqu.And(
			goqu.I("id").Eq(trainingId),
			goqu.I("company_id").Eq(companyId),
		)).
		ScanStructContext(ctx, training)

	if err != nil {
		return nil, err
	}

	if !found {
		return nil, ErrTrainingNotFound
	}

	return training, nil
}

func (r *goquRepository) SaveTraining(ctx context.Context, training *Training) (*Training, error) {
	tx, err := r.builder.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}

	defer tx.Rollback()

	if _, err := r.accountingRepo.RegisterTransaction(
		&database.DB{TxDatabase: tx},
		accounting.Transaction{
			Value:          -int(training.Investment),
			Description:    "Staff training",
			Classification: accounting.STAFF_TRAINING,
		},
		training.CompanyId,
	); err != nil {
		return nil, err
	}

	result, err := tx.
		Insert(goqu.T("trainings")).
		Rows(training).
		Executor().
		Exec()

	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	training.Id = uint64(id)

	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return training, nil
}

func (r *goquRepository) UpdateTraining(ctx context.Context, training *Training) error {
	tx, err := r.builder.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	defer tx.Rollback()

	_, err = tx.
		Update(goqu.T("trainings")).
		Set(training).
		Where(goqu.I("id").Eq(training.Id)).
		Executor().
		Exec()

	if err != nil {
		return err
	}

	_, err = tx.
		Update(goqu.T("research_staff")).
		Set(goqu.Record{
			"skill": goqu.Case().When(
				goqu.L("(? + ?) > 100", goqu.I("skill"), training.Result),
				100,
			).Else(goqu.L("? + ?", goqu.I("skill"), training.Result)),
		}).
		Where(goqu.I("id").Eq(training.StaffId)).
		Executor().
		Exec()

	if err != nil {
		return err
	}

	return tx.Commit()
}

func (r *goquRepository) GetSalaries(ctx context.Context, end time.Time) ([]*Payroll, error) {
	salaries := make([]*Payroll, 0)

	err := r.builder.
		Select(
			goqu.I("rs.company_id"),
			goqu.COALESCE(goqu.SUM(goqu.I("rs.salary")), 0).As("total"),
			goqu.COALESCE(goqu.SUM(goqu.I("t.value")), 0).As("available_cash"),
		).
		From(goqu.I("research_staff").As("rs")).
		InnerJoin(
			goqu.T("companies").As("c"),
			goqu.On(goqu.And(
				goqu.I("c.id").Eq(goqu.I("rs.company_id")),
				goqu.I("c.deleted_at").IsNull(),
				goqu.I("c.blocked_at").IsNull(),
			)),
		).
		LeftJoin(
			goqu.T("transactions").As("t"),
			goqu.On(goqu.I("t.company_id").Eq(goqu.I("c.id"))),
		).
		Where(goqu.I("rs.hired_at").Lte(end)).
		GroupBy(goqu.I("rs.company_id")).
		ScanStructsContext(ctx, &salaries)

	if err != nil {
		return nil, err
	}

	return salaries, nil
}

func (r *goquRepository) GoOnStrike(ctx context.Context, companyId uint64) error {
	_, err := r.builder.
		Update(goqu.T("research_staff")).
		Set(goqu.Record{
			"on_strike_until": time.Now().Add(7 * 24 * time.Hour),
		}).
		Where(goqu.I("company_id").Eq(companyId)).
		Prepared(true).
		Executor().
		ExecContext(ctx)

	return err
}

func (r *goquRepository) PaySalaries(ctx context.Context, total int, companyId uint64) error {
	tx, err := r.builder.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	defer tx.Rollback()

	if _, err := tx.
		Update(goqu.T("research_staff")).
		Set(goqu.Record{"on_strike_until": nil}).
		Where(goqu.I("company_id").Eq(companyId)).
		Executor().
		Exec(); err != nil {
		return err
	}

	if _, err := r.accountingRepo.RegisterTransaction(
		&database.DB{TxDatabase: tx},
		accounting.Transaction{
			Value:          -total,
			Description:    "Research staff salaries",
			Classification: accounting.RESEARCH_STAFF_SALARIES,
		},
		companyId,
	); err != nil {
		return err
	}

	return tx.Commit()
}
