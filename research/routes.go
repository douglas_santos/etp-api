package research

import (
	"api/auth"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func CreateEndpoints(e *echo.Echo, service Service) {
	group := e.Group("/research")

	group.GET("/categories/me", func(c echo.Context) error {
		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		categories, err := service.GetCategories(c.Request().Context(), companyId)
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, categories)
	})

	group.POST("/:resourceId", func(c echo.Context) error {
		resourceId, err := strconv.ParseUint(c.Param("resourceId"), 10, 64)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		body := struct {
			StaffIds []uint64 `json:"staff_ids"`
		}{}
		if err := c.Bind(&body); err != nil {
			return err
		}

		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		research, err := service.StartResearch(c.Request().Context(), body.StaffIds, resourceId, companyId)
		if err != nil {
			println(err.Error())
			return err
		}

		return c.JSON(http.StatusOK, research)
	})
}
