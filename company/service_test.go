package company_test

import (
	"api/company"
	"context"
	"testing"
	"time"
)

func TestCompanyService(t *testing.T) {
	service := company.NewService(company.NewFakeRepository())

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	t.Run("PurchaseTerrain", func(t *testing.T) {
		t.Run("should validate cash", func(t *testing.T) {
			err := service.PurchaseTerrain(ctx, 1, 10)
			expectedError := "not enough cash"

			if err.Error() != expectedError {
				t.Errorf("expected error \"%s\", got \"%s\"", expectedError, err)
			}
		})

		t.Run("should validate company", func(t *testing.T) {
			err := service.PurchaseTerrain(ctx, 10, 0)
			expectedError := "company not found"

			if err.Error() != expectedError {
				t.Errorf("expected error \"%s\", got \"%s\"", expectedError, err)
			}
		})
	})

	t.Run("Login", func(t *testing.T) {
		t.Run("invalid email", func(t *testing.T) {
			_, err := service.Login(ctx, company.Credentials{
				Email: "admin@email.com",
				Pass:  "password",
			})

			if err == nil {
				t.Fatal("should not login with invalid email")
			}

			expectedError := "invalid credentials"
			if err.Error() != expectedError {
				t.Errorf("expected error \"%s\", got \"%s\"", expectedError, err)
			}
		})

		t.Run("invalid password", func(t *testing.T) {
			_, err := service.Login(ctx, company.Credentials{
				Email: "admin@test.com",
				Pass:  "passwordi",
			})

			if err == nil {
				t.Fatal("should not login with invalid email")
			}

			expectedError := "invalid credentials"
			if err.Error() != expectedError {
				t.Errorf("expected error \"%s\", got \"%s\"", expectedError, err)
			}
		})

		t.Run("update last login", func(t *testing.T) {
			token, err := service.Login(ctx, company.Credentials{
				Email: "admin@test.com",
				Pass:  "password",
			})

			if err != nil {
				t.Fatalf("could not login: %s", err)
			}

			if token == "" {
				t.Fatal("should return a token")
			}

			company, err := service.GetById(ctx, 1)
			if err != nil {
				t.Fatalf("could not get company: %s", err)
			}

			if company.LastLogin == nil {
				t.Error("should update last login")
			}
		})
	})
}
