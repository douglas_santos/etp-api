package building_test

import (
	"api/building"
	companyBuilding "api/company/building"
	"api/scheduler"
	"api/warehouse"
	"context"
	"log"
	"testing"
	"time"
)

func TestScheduledService(t *testing.T) {
	repository := companyBuilding.NewFakeBuildingRepository()
	warehouseSvc := warehouse.NewService(warehouse.NewFakeRepository())
	buildingSvc := building.NewService(building.NewFakeRepository())
	service := companyBuilding.NewBuildingService(repository, warehouseSvc, buildingSvc)

	logger := log.Default()
	emitter := scheduler.NewEmitter(logger)
	timer := scheduler.NewScheduler(scheduler.NewInMemoryRepository(), emitter, logger)
	scheduled := companyBuilding.NewScheduledBuildingService(service, timer)

	t.Run("HandleBuildingConstructed", func(t *testing.T) {
		emitter.Subscribe(scheduled.HandleBuildingConstructed)

		completesAt := time.Now().Add(100 * time.Millisecond)
		err := timer.Schedule("test", completesAt, companyBuilding.BuildingConstructed{
			CompanyId: 2,
			Building: &companyBuilding.CompanyBuilding{
				Building: &building.Building{
					Id:   1,
					Name: "Plantation",
				},
				Level:       0,
				Id:          6,
				CompletesAt: &completesAt,
			},
		})

		if err != nil {
			t.Fatalf("could not schedule: %s", err)
		}

		time.Sleep(150 * time.Millisecond)
		timer.Remove("test")

		constructed, err := scheduled.GetBuilding(context.TODO(), 2, 6)
		if err != nil {
			t.Fatalf("could not get building: %s", err)
		}

		if constructed.CompletesAt != nil {
			t.Error("should be completed")
		}
	})
}
