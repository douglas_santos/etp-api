package building

import (
	"api/scheduler"
	"context"
	"fmt"
	"time"
)

type (
	BuildingConstructed struct {
		CompanyId uint64
		Building  *CompanyBuilding
	}

	ScheduledBuildingService interface {
		BuildingService
		HandleBuildingConstructed(payload BuildingConstructed) error
	}

	scheduledBuildingService struct {
		timer   *scheduler.Scheduler
		service BuildingService
	}
)

func NewScheduledBuildingService(buildingSvc BuildingService, timer *scheduler.Scheduler) ScheduledBuildingService {
	return &scheduledBuildingService{
		timer:   timer,
		service: buildingSvc,
	}
}

func (s *scheduledBuildingService) GetBuilding(ctx context.Context, companyId, buildingId uint64) (*CompanyBuilding, error) {
	return s.service.GetBuilding(ctx, companyId, buildingId)
}

func (s *scheduledBuildingService) GetBuildings(ctx context.Context, companyId uint64) ([]*CompanyBuilding, error) {
	return s.service.GetBuildings(ctx, companyId)
}

func (s *scheduledBuildingService) Update(ctx context.Context, companyId uint64, companyBuilding *CompanyBuilding) error {
	return s.service.Update(ctx, companyId, companyBuilding)
}

func (s *scheduledBuildingService) AddBuilding(ctx context.Context, companyId, buildingId uint64, position uint8) (*CompanyBuilding, error) {
	companyBuilding, err := s.service.AddBuilding(ctx, companyId, buildingId, position)
	if err != nil {
		return nil, err
	}

	s.timer.Schedule(fmt.Sprintf("CONSTRUCT_BUILDING_%d", companyBuilding.Id), *companyBuilding.CompletesAt, BuildingConstructed{
		CompanyId: companyId,
		Building:  companyBuilding,
	})

	return companyBuilding, nil
}

func (s *scheduledBuildingService) Demolish(ctx context.Context, companyId, buildingId uint64) error {
	err := s.service.Demolish(ctx, companyId, buildingId)
	if err != nil {
		return err
	}

	s.timer.Remove(buildingId)
	return nil
}

func (s *scheduledBuildingService) Upgrade(ctx context.Context, companyId, buildingId uint64) (*CompanyBuilding, error) {
	companyBuilding, err := s.service.Upgrade(ctx, companyId, buildingId)
	if err != nil {
		return nil, err
	}

	s.timer.Schedule(fmt.Sprintf("UPGRADE_BUILDING_%d", buildingId), *companyBuilding.CompletesAt, BuildingConstructed{
		CompanyId: companyId,
		Building:  companyBuilding,
	})

	return companyBuilding, nil
}

func (s *scheduledBuildingService) HandleBuildingConstructed(payload BuildingConstructed) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	payload.Building.CompletesAt = nil
	return s.service.Update(ctx, payload.CompanyId, payload.Building)
}
