package production

import (
	"api/resource"
	"api/scheduler"
	"api/warehouse"
	"context"
	"fmt"
	"time"
)

type (
	ProductionFinished struct {
		CompanyId    uint64
		BuildingId   uint64
		ProductionId uint64
	}

	ScheduledProductionService interface {
		ProductionService
		HandleProductionFinished(payload ProductionFinished) error
	}

	scheduledProductionService struct {
		timer   *scheduler.Scheduler
		service ProductionService
	}
)

func NewScheduledProductionService(service ProductionService, timer *scheduler.Scheduler) ScheduledProductionService {
	return &scheduledProductionService{
		timer:   timer,
		service: service,
	}
}

func (s *scheduledProductionService) HandleProductionFinished(payload ProductionFinished) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	_, err := s.CollectResource(ctx, payload.CompanyId, payload.BuildingId, payload.ProductionId)
	return err
}

func (s *scheduledProductionService) Produce(ctx context.Context, companyId, companyBuildingId uint64, item *resource.Item) (*Production, error) {
	startedProduction, err := s.service.Produce(ctx, companyId, companyBuildingId, item)
	if err != nil {
		return nil, err
	}

	s.timer.Schedule(fmt.Sprintf("PRODUCTION_%d", startedProduction.Id), startedProduction.FinishesAt, ProductionFinished{
		CompanyId:    companyId,
		BuildingId:   companyBuildingId,
		ProductionId: startedProduction.Id,
	})

	return startedProduction, nil
}

func (s *scheduledProductionService) CancelProduction(ctx context.Context, companyId, companyBuildingId, productionId uint64) error {
	err := s.service.CancelProduction(ctx, companyId, companyBuildingId, productionId)
	if err != nil {
		return err
	}

	s.timer.Remove(fmt.Sprintf("PRODUCTION_%d", productionId))
	return nil
}

func (s *scheduledProductionService) CollectResource(ctx context.Context, companyId, companyBuildingId, productionId uint64) (*warehouse.StockItem, error) {
	return s.service.CollectResource(ctx, companyId, companyBuildingId, productionId)
}
