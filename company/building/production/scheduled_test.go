package production_test

import (
	"api/building"
	"api/company"
	companyBuilding "api/company/building"
	"api/company/building/production"
	"api/research"
	"api/scheduler"
	"api/warehouse"
	"context"
	"log"
	"testing"
	"time"
)

func TestScheduledService(t *testing.T) {
	companySvc := company.NewService(company.NewFakeRepository())
	warehouseSvc := warehouse.NewService(warehouse.NewFakeRepository())

	buildingSvc := building.NewService(building.NewFakeRepository())
	companyBuildingSvc := companyBuilding.NewBuildingService(companyBuilding.NewFakeBuildingRepository(), warehouseSvc, buildingSvc)

	logger := log.Default()
	emitter := scheduler.NewEmitter(logger)
	timer := scheduler.NewScheduler(scheduler.NewInMemoryRepository(), emitter, logger)

	researchSvc := research.NewService(research.NewFakeRepository(), companySvc, timer)
	repository := production.NewFakeProductionRepository()
	service := production.NewProductionService(repository, companySvc, companyBuildingSvc, warehouseSvc, researchSvc)
	scheduled := production.NewScheduledProductionService(service, timer)

	t.Run("HandleProductionFinished", func(t *testing.T) {
		if err := emitter.Subscribe(scheduled.HandleProductionFinished); err != nil {
			t.Fatalf("could not subscribe: %s", err)
		}

		timer.Schedule("test", time.Now().Add(100*time.Millisecond), production.ProductionFinished{
			CompanyId:    2,
			BuildingId:   7,
			ProductionId: 3,
		})

		time.Sleep(150 * time.Millisecond)

		production, err := repository.GetProduction(context.TODO(), 3, 7, 2)
		if err != nil {
			t.Fatalf("could not get production: %s", err)
		}

		if production.LastCollection == nil {
			t.Error("should have collected")
		}
	})
}
