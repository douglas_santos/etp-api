package production

import (
	"api/building"
	companyBuilding "api/company/building"
	"api/resource"
	"api/warehouse"
	"context"
	"sync"
	"time"
)

type fakeProductionRepository struct {
	mutex  sync.Mutex
	lastId uint64
	data   map[uint64]map[uint64]*Production
}

func NewFakeProductionRepository() ProductionRepository {
	data := map[uint64]map[uint64]*Production{
		1: {
			1: {
				Id: 1,
				Item: &resource.Item{
					Quality:  0,
					Qty:      2000,
					Resource: &resource.Resource{Id: 6},
				},
				ResourcesCost:  70500,
				FinishesAt:     time.Now().Add(time.Hour),
				StartedAt:      time.Now().Add(-time.Hour),
				ProductionCost: 60,
				SourcingCost:   4704,
				Building: &companyBuilding.CompanyBuilding{
					Id:    1,
					Level: 1,
					Building: &building.Building{
						Id:        4,
						Name:      "Factory",
						WagesHour: 10,
						AdminHour: 50,
						Resources: []*building.BuildingResource{
							{
								QtyPerHours: 1000,
								Resource: &resource.Resource{
									Id:   6,
									Name: "Iron bar",
									Requirements: []*resource.Requirement{
										{Qty: 1500, Resource: &resource.Resource{Id: 3}},
									},
								},
							},
						},
					},
				},
			},
			2: {
				Id: 2,
				Item: &resource.Item{
					Quality:  0,
					Qty:      2000,
					Resource: &resource.Resource{Id: 6},
				},
				ResourcesCost:  70500,
				FinishesAt:     time.Now().Add(time.Hour),
				StartedAt:      time.Now().Add(-time.Hour),
				ProductionCost: 60,
				SourcingCost:   4704,
				Building: &companyBuilding.CompanyBuilding{
					Level: 1,
					Building: &building.Building{
						Id:        4,
						Name:      "Factory",
						WagesHour: 10,
						AdminHour: 50,
						Resources: []*building.BuildingResource{
							{
								QtyPerHours: 1000,
								Resource: &resource.Resource{
									Id:   6,
									Name: "Iron bar",
									Requirements: []*resource.Requirement{
										{Qty: 1500, Resource: &resource.Resource{Id: 3}},
									},
								},
							},
						},
					},
				},
			},
		},
		2: {
			3: {
				Id: 3,
				Item: &resource.Item{
					Quality:  0,
					Qty:      2000,
					Resource: &resource.Resource{Id: 1},
				},
				ResourcesCost:  70500,
				FinishesAt:     time.Now().Add(time.Hour),
				StartedAt:      time.Now().Add(-time.Hour),
				ProductionCost: 60,
				SourcingCost:   4704,
				Building: &companyBuilding.CompanyBuilding{
					Id:    7,
					Level: 0,
					Building: &building.Building{
						Id:        1,
						Name:      "Plantation",
						WagesHour: 100,
						AdminHour: 500,
						Resources: []*building.BuildingResource{
							{
								QtyPerHours: 1000,
								Resource:    &resource.Resource{Id: 1, Name: "Seeds"},
							},
						},
					},
				},
			},
		},
	}
	return &fakeProductionRepository{lastId: 2, data: data}
}

func (r *fakeProductionRepository) GetProduction(ctx context.Context, productionId, buildingId, companyId uint64) (*Production, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	return r.data[companyId][productionId], nil
}

func (r *fakeProductionRepository) SaveProduction(ctx context.Context, production *Production, inventory *warehouse.Inventory, companyId uint64) (*Production, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	finishesAt := time.Now().Add(time.Hour)
	production.Building.BusyUntil = &finishesAt

	r.lastId++
	production.Id = r.lastId

	_, ok := r.data[companyId]
	if !ok {
		r.data[companyId] = make(map[uint64]*Production)
	}

	r.data[companyId][r.lastId] = production

	return production, nil
}

func (r *fakeProductionRepository) CancelProduction(ctx context.Context, production *Production, inventory *warehouse.Inventory) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	r.data[inventory.CompanyId][production.Id] = production
	return nil
}

func (r *fakeProductionRepository) CollectResource(ctx context.Context, production *Production, inventory *warehouse.Inventory) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	r.data[inventory.CompanyId][production.Id] = production
	return nil
}
