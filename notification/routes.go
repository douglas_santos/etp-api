package notification

import (
	"api/auth"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
)

func CreateEndpoints(e *echo.Echo, service Service, notifier Notifier, logger *log.Logger) {
	group := e.Group("/notifications")

	group.GET("", func(c echo.Context) error {
		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		notifications, err := service.GetNotifications(c.Request().Context(), int64(companyId))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, notifications)
	})

	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool { return true },
	}

	group.GET("/ws", func(c echo.Context) error {
		ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
		if err != nil {
			return err
		}

		var clientId int64
		client := &Client{Conn: ws}

		defer ws.Close()
		defer client.Close()

		for {
			_, message, err := ws.ReadMessage()
			if err != nil {
				logger.Printf("could not read message: %s", err)
				notifier.Disconnect(clientId)
				break
			}

			token, err := auth.ParseToken(string(message))
			if err != nil {
				logger.Printf("could not parse token: %s", err)
				continue
			}

			clientId, err := auth.GetIdFromToken(token)
			if err != nil {
				logger.Printf("could not extract ID from token: %s", err)
				continue
			}

			notifier.Connect(int64(clientId), client)
		}

		return nil
	})
}
