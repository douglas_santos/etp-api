package bonds_test

import (
	"api/accounting"
	"api/company"
	"api/database"
	"api/financing/bonds"
	"api/notification"
	"api/scheduler"
	"context"
	"log"
	"testing"
	"time"
)

func TestScheduledService(t *testing.T) {
	conn, err := database.GetConnection(database.SQLITE, "../../test.db")
	if err != nil {
		t.Fatalf("could not connect to database: %s", err)
	}

	tx, err := conn.DB.Begin()
	if err != nil {
		t.Fatalf("could not start transaction: %s", err)
	}

	defer tx.Rollback()

	if _, err := tx.Exec(`
        INSERT INTO companies (id, name, email, password) VALUES
        (1, "Coca-Cola", "coke@email.com", "aoeu"),
        (2, "Pepsi", "coke@email.com", "aoeu")
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO bonds (id, company_id, amount, interest_rate, canceled_at) VALUES
        (1, 1, 200000000, 0.15, NULL)
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO bonds_creditors (bond_id, company_id, principal, principal_paid, interest_rate, payable_from, delayed_payments) VALUES
        (1, 2, 100000000, 0, 0.15, "2024-12-12 00:00:00", 1)
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO transactions (company_id, value, classification_id) VALUES
        (1, 100000000, 2)
    `); err != nil {
		t.Fatalf("could not seed database: %s", err)
	}

	if err := tx.Commit(); err != nil {
		t.Fatalf("could not commit transaction: %s", err)
	}

	t.Cleanup(func() {
		if _, err := conn.DB.Exec(`DELETE FROM schedules`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM transactions`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM bonds_creditors`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM bonds`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
		if _, err := conn.DB.Exec(`DELETE FROM companies`); err != nil {
			t.Fatalf("could not cleanup database: %s", err)
		}
	})

	accountingRepo := accounting.NewRepository(conn)
	companyRepo := company.NewRepository(conn, accountingRepo)
	companySvc := company.NewService(companyRepo)
	service := bonds.NewService(bonds.NewRepository(conn, accountingRepo), companySvc, notification.NoOpNotifier(), log.Default())

	emitter := scheduler.NewEmitter(log.Default())
	repository := scheduler.NewRepository(conn)
	timer := scheduler.NewScheduler(repository, emitter, log.Default())
	scheduled := bonds.NewScheduledService(service, timer)

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	t.Run("HandleBondInterest", func(t *testing.T) {
		payload := bonds.BondInterest{
			Bond:     1,
			Creditor: 2,
		}

		emitter.Subscribe(scheduled.HandleBondInterest)
		timer.Interval("test", 100*time.Millisecond, payload)

		time.Sleep(150 * time.Millisecond)
		timer.Remove("test")

		company, err := companyRepo.GetById(ctx, 2)
		if err != nil {
			t.Errorf("could not get company: %s", err)
		}

		if company.AvailableCash != 150_000_00 {
			t.Errorf("expected cash %d, got %d", 150_000_00, company.AvailableCash)
		}
	})
}
