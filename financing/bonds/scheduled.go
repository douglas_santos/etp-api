package bonds

import (
	"api/scheduler"
	"context"
	"fmt"
	"time"
)

type (
	ScheduledService interface {
		Service
		HandleBondInterest(payload BondInterest) error
	}

	scheduledService struct {
		service   Service
		scheduler *scheduler.Scheduler
	}

	BondInterest struct {
		Bond     int64
		Creditor uint64
	}
)

func NewScheduledService(service Service, scheduler *scheduler.Scheduler) ScheduledService {
	return &scheduledService{service, scheduler}
}

func (s *scheduledService) GetBonds(ctx context.Context, page, limit uint, companyId int64) ([]*Bond, error) {
	return s.service.GetBonds(ctx, page, limit, companyId)
}

func (s *scheduledService) GetIssuedBonds(ctx context.Context, companyId int64) ([]*Bond, error) {
	return s.service.GetIssuedBonds(ctx, companyId)
}

func (s *scheduledService) GetOwnedBonds(ctx context.Context, companyId int64) ([]*Creditor, error) {
	return s.service.GetOwnedBonds(ctx, companyId)
}

func (s *scheduledService) EmitBond(ctx context.Context, rate float64, amount, companyId int64) (*Bond, error) {
	return s.service.EmitBond(ctx, rate, amount, companyId)
}

func (s *scheduledService) CancelBond(ctx context.Context, bondId, companyId int64) error {
	return s.service.CancelBond(ctx, bondId, companyId)
}

func (s *scheduledService) HandleBondInterest(payload BondInterest) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	return s.PayBondInterest(ctx, payload.Creditor, payload.Bond)
}

func (s *scheduledService) BuyBond(ctx context.Context, amount, bondId, companyId int64) (*Bond, *Creditor, error) {
	bond, creditor, err := s.service.BuyBond(ctx, amount, bondId, companyId)
	if err != nil {
		return nil, nil, err
	}

	s.scheduler.Interval(fmt.Sprintf("BOND_%d_CREDITOR_%d", bond.Id, creditor.Id), Week, BondInterest{
		Creditor: creditor.Id,
		Bond:     bond.Id,
	})

	return bond, creditor, nil
}

func (s *scheduledService) PayBondInterest(ctx context.Context, creditorId uint64, bondId int64) error {
	return s.service.PayBondInterest(ctx, creditorId, bondId)
}

func (s *scheduledService) BuyBackBond(ctx context.Context, amount, bondId, creditorId, companyId int64) (*Creditor, error) {
	return s.service.BuyBackBond(ctx, amount, bondId, creditorId, companyId)
}
