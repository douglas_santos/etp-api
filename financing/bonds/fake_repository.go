package bonds

import (
	"api/company"
	"context"
	"time"
)

type fakeRepository struct {
	lastId int64
	bonds  map[int64]*Bond

	companyRepo company.Repository
}

func NewFakeRepository(companyRepo company.Repository) Repository {
	return &fakeRepository{
		lastId:      3,
		companyRepo: companyRepo,
		bonds: map[int64]*Bond{
			1: {
				Id:           1,
				Amount:       1_000_000_00,
				InterestRate: 0.1,
				CompanyId:    1,
				Purchased:    500_000_00,
				Creditors: []*Creditor{
					{
						Company:         &company.Company{Id: 2, Name: "Coca-Cola"},
						Principal:       500_000_00,
						InterestRate:    0.1,
						InterestPaid:    100_000_00,
						DelayedPayments: 0,
						PrincipalPaid:   100_000_00,
					},
				},
			},
			2: {
				Id:           2,
				Amount:       2_000_000_00,
				InterestRate: 0.1,
				CompanyId:    3,
				Purchased:    2_000_000_00,
				Creditors: []*Creditor{
					{
						Company:         &company.Company{Id: 1},
						Principal:       500_000_00,
						InterestRate:    0.1,
						InterestPaid:    100_000_00,
						DelayedPayments: 0,
						PrincipalPaid:   100_000_00,
					},
					{
						Company:         &company.Company{Id: 4},
						Principal:       1_500_000_00,
						InterestRate:    0.1,
						InterestPaid:    0,
						DelayedPayments: 0,
						PrincipalPaid:   0,
					},
				},
			},
			3: {
				Id:           3,
				Amount:       1_500_000_00,
				InterestRate: 0.1,
				CompanyId:    3,
				Creditors: []*Creditor{
					{
						Company:         &company.Company{Id: 4},
						Principal:       1_500_000_00,
						InterestRate:    0.1,
						InterestPaid:    0,
						DelayedPayments: 0,
						PrincipalPaid:   0,
					},
				},
			},
		},
	}
}

func (r *fakeRepository) GetBonds(ctx context.Context, page, limit uint, companyId int64) ([]*Bond, error) {
	index := 0
	bonds := make([]*Bond, 0, limit)
	for _, bond := range r.bonds {
		if len(bonds) >= int(limit) {
			break
		}

		if int(page*limit) <= index && bond.CompanyId != companyId {
			bonds = append(bonds, bond)
		}

		index++
	}
	return bonds, nil
}

func (r *fakeRepository) GetIssuedBonds(ctx context.Context, companyId int64) ([]*Bond, error) {
	bonds := make([]*Bond, 0)
	for _, bond := range r.bonds {
		if bond.CompanyId == companyId {
			bonds = append(bonds, bond)
		}
	}
	return bonds, nil
}

func (r *fakeRepository) GetOwnedBonds(ctx context.Context, companyId int64) ([]*Creditor, error) {
	creditors := make([]*Creditor, 0)
	for _, bond := range r.bonds {
		for _, creditor := range bond.Creditors {
			if creditor.Company.Id == uint64(companyId) {
				creditors = append(creditors, creditor)
			}
		}
	}
	return creditors, nil
}

func (r *fakeRepository) GetBond(ctx context.Context, bondId int64) (*Bond, error) {
	bond, ok := r.bonds[bondId]
	if !ok {
		return nil, ErrBondNotFound
	}
	return bond, nil
}

func (r *fakeRepository) SaveBond(ctx context.Context, bond *Bond) (*Bond, error) {
	r.lastId++
	bond.Id = r.lastId
	r.bonds[bond.Id] = bond
	return bond, nil
}

func (r *fakeRepository) PayBondInterest(ctx context.Context, bond *Bond, creditor *Creditor) error {
	creditor.DelayedPayments = 0
	creditor.InterestPaid += creditor.GetInterest()

	company, _ := r.companyRepo.GetById(ctx, uint64(bond.CompanyId))
	company.AvailableCash -= int(creditor.GetInterest())

	return nil
}

func (r *fakeRepository) SaveCreditor(ctx context.Context, bond *Bond, creditor *Creditor) (*Creditor, error) {
	creditor.AvailableCash -= int(creditor.Principal)
	return creditor, nil
}

func (r *fakeRepository) BuyBackBond(ctx context.Context, amount int64, creditor *Creditor, bond *Bond) (*Creditor, error) {
	creditor.PrincipalPaid += amount
	return creditor, nil
}

func (r *fakeRepository) CancelBond(ctx context.Context, bondId int64) error {
	now := time.Now()
	r.bonds[bondId].CanceledAt = &now
	return nil
}
