package bonds

import (
	"api/auth"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func CreateEndpoints(group *echo.Group, service Service) {
	group.GET("/bonds/issued", func(c echo.Context) error {
		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		bonds, err := service.GetIssuedBonds(c.Request().Context(), int64(companyId))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, bonds)
	})

	group.GET("/bonds/owned", func(c echo.Context) error {
		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		bonds, err := service.GetOwnedBonds(c.Request().Context(), int64(companyId))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, bonds)
	})

	group.GET("/bonds", func(c echo.Context) error {
		page, err := strconv.ParseUint(c.QueryParam("page"), 10, 64)
		if err != nil {
			page = 1
		}

		limit, err := strconv.ParseUint(c.QueryParam("limit"), 10, 64)
		if err != nil {
			limit = 50
		}

		authenticatedId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		bonds, err := service.GetBonds(c.Request().Context(), uint(page-1), uint(limit), int64(authenticatedId))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, bonds)
	})

	group.POST("/bonds", func(c echo.Context) error {
		request := struct {
			Rate   float64 `json:"rate" validate:"required"`
			Amount int64   `json:"amount" validate:"required"`
		}{}

		if err := c.Bind(&request); err != nil {
			return err
		}

		if err := c.Validate(&request); err != nil {
			return err
		}

		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		bond, err := service.EmitBond(c.Request().Context(), request.Rate, request.Amount, int64(companyId))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, bond)
	})

	group.PUT("/bonds/:bondId", func(c echo.Context) error {
		bondId, err := strconv.ParseInt(c.Param("bondId"), 10, 64)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		request := struct {
			Amount     int64 `json:"amount" validate:"required"`
			CreditorId int64 `json:"creditor_id" validate:"required"`
		}{}

		if err := c.Bind(&request); err != nil {
			return err
		}

		if err := c.Validate(&request); err != nil {
			return err
		}

		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		creditor, err := service.BuyBackBond(c.Request().Context(), request.Amount, bondId, request.CreditorId, int64(companyId))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, creditor)
	})

	group.POST("/bonds/:bondId", func(c echo.Context) error {
		bondId, err := strconv.ParseInt(c.Param("bondId"), 10, 64)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		request := struct {
			Amount int64 `json:"amount" validate:"required"`
		}{}

		if err := c.Bind(&request); err != nil {
			return err
		}

		if err := c.Validate(&request); err != nil {
			return err
		}

		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		_, creditor, err := service.BuyBond(c.Request().Context(), request.Amount, bondId, int64(companyId))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, creditor)
	})

	group.DELETE("/bonds/:bondId", func(c echo.Context) error {
		bondId, err := strconv.ParseInt(c.Param("bondId"), 10, 64)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		if err := service.CancelBond(c.Request().Context(), bondId, int64(companyId)); err != nil {
			return err
		}

		return c.NoContent(http.StatusNoContent)
	})
}
