package loans_test

import (
	"api/company"
	"api/financing"
	"api/financing/loans"
	"api/notification"
	"api/scheduler"
	"context"
	"fmt"
	"log"
	"testing"
	"time"
)

func TestScheduledService(t *testing.T) {
	companyRepo := company.NewFakeRepository()
	companySvc := company.NewService(companyRepo)

	logger := log.Default()
	notifier := notification.NoOpNotifier()

	financingSvc := financing.NewService(financing.NewFakeRepository(), notifier, logger)
	service := loans.NewService(loans.NewFakeRepository(companyRepo), companySvc, financingSvc, notifier, logger)

	emitter := scheduler.NewEmitter(logger)
	timer := scheduler.NewScheduler(scheduler.NewInMemoryRepository(), emitter, logger)

	scheduled := loans.NewScheduledService(service, timer)

	t.Run("HandleLoanInterest", func(t *testing.T) {
		err := emitter.Subscribe(scheduled.HandleLoanInterest)
		if err != nil {
			t.Fatalf("could not subscribe: %s", err)
		}

		err = timer.Interval("LOAN_2", 100*time.Millisecond, loans.LoanInterest{
			LoanId:    2,
			CompanyId: 3,
		})

		if err != nil {
			t.Fatalf("could not set interval: %s", err)
		}

		time.Sleep(150 * time.Millisecond)
		timer.Remove("LOAN_2")

		bonds, err := scheduled.GetLoans(context.TODO(), 3)
		if err != nil {
			t.Fatalf("could not get loans: %s", err)
		}

		if bonds[0].InterestPaid != 50_000_00 {
			t.Errorf("expected interest paid of %d, got %d", 50_000_00, bonds[0].InterestPaid)
		}
	})

	t.Run("should clear timer", func(t *testing.T) {
		run := make(chan bool)

		timer.Interval(fmt.Sprintf("LOAN_%d", int64(4)), 100*time.Millisecond, loans.LoanInterest{
			LoanId:    4,
			CompanyId: 1,
		})

		emitter.Subscribe(func(payload loans.LoanInterest) error {
			run <- true
			return nil
		})

		ok, err := scheduled.PayLoanInterest(context.TODO(), 4, 1)
		if err != nil {
			t.Fatalf("could not pay interest: %s", err)
		}

		if !ok {
			t.Error("should be ok")
		}

		select {
		case <-time.After(150 * time.Millisecond):
		case <-run:
			t.Error("should not execute callback")
		}
	})
}
