package scheduler_test

import (
	"api/scheduler"
	"errors"
	"log"
	"testing"
)

func TestEmitter(t *testing.T) {
	var total int
	count := make(chan int, 2)
	emitter := scheduler.NewEmitter(log.Default())

	type ProductionFinished struct {
		Sum int
	}

	emitter.Subscribe(func(payload ProductionFinished) error {
		count <- payload.Sum
		return nil
	})

	emitter.Subscribe(func(payload ProductionFinished) error {
		return errors.New("testing")
	})

	emitter.Emit(ProductionFinished{Sum: 1})
	total += <-count

	emitter.Emit(ProductionFinished{Sum: 2})
	total += <-count

	if total != 3 {
		t.Errorf("expected total %d, got %d", 3, total)
	}
}
