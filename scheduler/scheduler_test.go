package scheduler_test

import (
	// "api/research/staff"
	"api/research/staff"
	"api/scheduler"
	"log"
	"testing"
	"time"
)

type (
	Interval struct{ C int }
	Test     struct{ C int }
	Repeat   struct{ C int }
)

func TestScheduler(t *testing.T) {
	logger := log.Default()
	emitter := scheduler.NewEmitter(logger)
	timer := scheduler.NewScheduler(scheduler.NewInMemoryRepository(), emitter, logger)

	t.Run("Init", func(t *testing.T) {
		fooChan := make(chan int)
		barChan := make(chan int)
		bazChan := make(chan int)
		intChan := make(chan int)

		emitter.Subscribe(func(payload scheduler.Foo) error {
			fooChan <- payload.C
			return nil
		})

		emitter.Subscribe(func(payload scheduler.Bar) error {
			barChan <- payload.C
			return nil
		})

		emitter.Subscribe(func(payload scheduler.Baz) error {
			bazChan <- payload.C
			return nil
		})

		emitter.Subscribe(func(payload scheduler.Int) error {
			intChan <- payload.C
			return nil
		})

		go timer.Init()

		fooCounter := 0
		barCounter := 0
		bazCounter := 0
		intCounter := 0

		for fooCounter != 2 || barCounter != 1 || bazCounter != 2 {
			select {
			case c := <-fooChan:
				fooCounter += c
			case c := <-barChan:
				barCounter += c
			case c := <-bazChan:
				bazCounter += c
			case c := <-intChan:
				intCounter += c
			}
		}

		if intCounter != 18 {
			t.Errorf("expected interval counter of %d, got %d", 18, intCounter)
		}
	})

	t.Run("Schedule", func(t *testing.T) {
		ch := make(chan int)

		emitter.Subscribe(func(payload staff.OfferExpired) error {
			ch <- int(payload.StaffId)
			return nil
		})

		timer.Schedule(69, time.Now().Add(200*time.Millisecond), staff.OfferExpired{
			StaffId:   69,
			CompanyId: 1,
		})

		select {
		case c := <-ch:
			if c != 69 {
				t.Errorf("expected 69, got %d", c)
			}
		case <-time.After(250 * time.Millisecond):
			t.Fatal("did not execute callback")
		}
	})

	t.Run("Remove", func(t *testing.T) {
		t.Run("schedule", func(t *testing.T) {
			ch := make(chan int)

			err := emitter.Subscribe(func(payload Test) error {
				ch <- payload.C
				return nil
			})
			if err != nil {
				t.Fatalf("could not subscribe: %s", err)
			}

			timer.Schedule(420, time.Now().Add(100*time.Millisecond), Test{C: 420})
			timer.Remove(420)

			select {
			case <-ch:
				t.Fatal("should not execute callback")
			case <-time.After(150 * time.Millisecond):
			}
		})

		t.Run("interval", func(t *testing.T) {
			count := make(chan int)

			err := emitter.Subscribe(func(payload Interval) error {
				count <- payload.C
				return nil
			})
			if err != nil {
				t.Fatalf("could not subscribe: %s", err)
			}

			timer.Interval(42069, 100*time.Millisecond, Interval{C: 420})
			timer.Remove(42069)

			select {
			case <-count:
				t.Fatal("should not execute callback")
			case <-time.After(150 * time.Millisecond):
			}
		})
	})

	t.Run("Repeat", func(t *testing.T) {
		count := make(chan int, 1)

		emitter.Subscribe(func(payload Repeat) error {
			count <- <-count + payload.C
			return nil
		})

		count <- 0
		timer.Interval(4206969, 100*time.Millisecond, Repeat{C: 1})

		time.Sleep(205 * time.Millisecond)

		if <-count != 2 {
			t.Errorf("expected count %d, got %d", 2, count)
		}
	})
}
