package scheduler

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"sync"
	"time"
)

type (
	Scheduler struct {
		timers  *sync.Map
		tickers *sync.Map

		logger     *log.Logger
		emitter    Emitter
		repository Repository
	}
)

func NewScheduler(repository Repository, emitter Emitter, logger *log.Logger) *Scheduler {
	return &Scheduler{
		timers:  &sync.Map{},
		tickers: &sync.Map{},

		logger:     logger,
		emitter:    emitter,
		repository: repository,
	}
}

func (s *Scheduler) Init() {
	schedules, err := s.repository.LoadSchedules()
	if err != nil {
		s.logger.Printf("could not load schedules: %s", err)
		return
	}

	for _, schedule := range schedules {
		if schedule.Duration != nil {
			s.startInterval(schedule)
		} else if schedule.FinishesAt != nil {
			// if the event is in the past, publish immediately
			if schedule.FinishesAt.Compare(time.Now()) <= 0 {
				if err := s.dispatch(schedule.Payload); err != nil {
					s.logger.Printf("Init: could not execute callback(%s): %s", schedule.EventId, err)
				}

				if err := s.repository.UpdateStatus(schedule.Id, STATUS_EXECUTED); err != nil {
					s.logger.Printf("could not update schedule status: %s", err)
				}
			} else {
				s.startTimer(schedule)
			}
		}
	}
}

func (s *Scheduler) Schedule(id any, finishesAt time.Time, payload any) error {
	if _, ok := s.timers.Load(id); ok {
		return fmt.Errorf("event already scheduled: %s", id)
	}

	encoded, _ := s.encodePayload(payload)
	schedule, err := s.repository.SaveSchedule(id, finishesAt, encoded)
	if err != nil {
		return err
	}

	s.startTimer(schedule)
	return nil
}

func (s *Scheduler) startTimer(schedule *Schedule) {
	s.timers.Store(schedule.EventId, time.AfterFunc(time.Until(*schedule.FinishesAt), func() {
		if err := s.dispatch(schedule.Payload); err != nil {
			s.logger.Printf("Timer: could not execute callback(%s): %s", schedule.EventId, err)
		}

		if err := s.repository.UpdateStatus(schedule.Id, STATUS_EXECUTED); err != nil {
			s.logger.Printf("could not update schedule status: %s", err)
		}

		s.timers.Delete(schedule.EventId)
	}))
}

func (s *Scheduler) Remove(id any) {
	if timer, found := s.timers.LoadAndDelete(id); found {
		if !timer.(*time.Timer).Stop() {
			<-timer.(*time.Timer).C
		}
	}

	if ticker, found := s.tickers.LoadAndDelete(id); found {
		ticker.(*time.Ticker).Stop()
	}

	if err := s.repository.CancelEventId(id); err != nil {
		s.logger.Printf("could not cancel schedule: %s", err)
	}
}

func (s *Scheduler) Interval(id any, duration time.Duration, payload any) error {
	encoded, _ := s.encodePayload(payload)
	interval, err := s.repository.SaveInterval(id, duration, encoded)
	if err != nil {
		return err
	}
	s.startInterval(interval)
	return nil
}

func (s *Scheduler) encodePayload(payload any) ([]byte, error) {
	buff := bytes.NewBuffer([]byte{})
	err := gob.NewEncoder(buff).Encode(&payload)

	return buff.Bytes(), err
}

func (s *Scheduler) startInterval(interval *Schedule) {
	ticker := time.NewTicker(*interval.Duration)
	s.tickers.Store(interval.EventId, ticker)

	go func(interval *Schedule) {
		for {
			select {
			case <-ticker.C:
				if err := s.dispatch(interval.Payload); err != nil {
					s.logger.Printf("could not execute interval(%s): %s", interval.EventId, err)
					break
				}
			}
		}
	}(interval)
}

func (s *Scheduler) dispatch(payload []byte) error {
	var data any
	buff := bytes.NewBuffer([]byte(payload))
	if err := gob.NewDecoder(buff).Decode(&data); err != nil {
		return err
	}
	return s.emitter.Emit(data)
}
