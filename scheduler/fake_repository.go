package scheduler

import (
	"bytes"
	"encoding/gob"
	"errors"
	"sync"
	"time"
)

type (
	inMemoryRepository struct {
		mutex sync.Mutex

		lastId    int64
		schedules map[int64]*Schedule
	}

	Foo struct{ C int }
	Bar struct{ C int }
	Baz struct{ C int }
	Int struct{ C int }
)

func createPayload(data any) []byte {
	gob.Register(data)
	buff := bytes.NewBuffer([]byte{})
	gob.NewEncoder(buff).Encode(&data)

	return buff.Bytes()
}

func NewInMemoryRepository() Repository {
	t1 := time.Now().Add(500 * time.Millisecond)
	t2 := time.Now().Add(600 * time.Millisecond)
	t3 := time.Now().Add(200 * time.Millisecond)
	t4 := time.Now().Add(-time.Second)
	duration := 90 * time.Millisecond

	schedules := map[int64]*Schedule{
		1: {Id: 1, EventId: 1, Payload: createPayload(Foo{C: 1}), FinishesAt: &t1},
		2: {Id: 2, EventId: 2, Payload: createPayload(Foo{C: 1}), FinishesAt: &t2},
		3: {Id: 3, EventId: 3, Payload: createPayload(Bar{C: 1}), FinishesAt: &t3},
		4: {Id: 4, EventId: 4, Payload: createPayload(Baz{C: 2}), FinishesAt: &t4},
		5: {Id: 5, EventId: 5, Payload: createPayload(Int{C: 3}), Duration: &duration},
	}

	return &inMemoryRepository{
		lastId:    5,
		schedules: schedules,
	}
}

func (r *inMemoryRepository) LoadSchedules() ([]*Schedule, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	schedules := make([]*Schedule, 0)
	for _, schedule := range r.schedules {
		schedules = append(schedules, schedule)
	}
	return schedules, nil
}

func (r *inMemoryRepository) UpdateStatus(scheduleId int64, status Status) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	schedule, ok := r.schedules[scheduleId]
	if !ok {
		return errors.New("schedule not found")
	}
	schedule.Status = status
	return nil
}

func (r *inMemoryRepository) CancelEventId(id any) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	for _, schedule := range r.schedules {
		if schedule.EventId == id {
			schedule.Status = STATUS_CANCELED
			return nil
		}
	}
	return errors.New("schedule for event not found")
}

func (r *inMemoryRepository) SaveSchedule(id any, finishesAt time.Time, payload []byte) (*Schedule, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	r.lastId++

	schedule := &Schedule{
		Id:         r.lastId,
		EventId:    id,
		Payload:    payload,
		FinishesAt: &finishesAt,
	}

	r.schedules[r.lastId] = schedule
	return schedule, nil
}

func (r *inMemoryRepository) SaveInterval(id any, duration time.Duration, payload []byte) (*Schedule, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	r.lastId++

	schedule := &Schedule{
		Id:       r.lastId,
		EventId:  id,
		Payload:  payload,
		Duration: &duration,
	}

	r.schedules[r.lastId] = schedule
	return schedule, nil
}
