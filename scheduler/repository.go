package scheduler

import (
	"api/database"
	"time"

	"github.com/doug-martin/goqu/v9"
)

const (
	STATUS_EXECUTED Status = "EXECUTED"
	STATUS_CANCELED        = "CANCELED"
	STATUS_PENDING         = "PENDING"
)

type (
	Status string

	Schedule struct {
		Id         int64          `db:"id" goqu:"skipinsert,skipupdate"`
		EventId    any            `db:"event_id"`
		FinishesAt *time.Time     `db:"finishes_at"`
		Duration   *time.Duration `db:"duration"`
		Payload    []byte         `db:"payload"`
		Status     Status         `db:"status"`
	}

	Repository interface {
		CancelEventId(id any) error
		LoadSchedules() ([]*Schedule, error)
		UpdateStatus(scheduleId int64, status Status) error
		SaveSchedule(id any, finishesAt time.Time, payload []byte) (*Schedule, error)
		SaveInterval(id any, duration time.Duration, payload []byte) (*Schedule, error)
	}

	goquRepository struct {
		builder *goqu.Database
	}
)

func NewRepository(conn *database.Connection) Repository {
	builder := goqu.New(conn.Driver, conn.DB)
	return &goquRepository{builder}
}

func (r *goquRepository) LoadSchedules() ([]*Schedule, error) {
	schedules := make([]*Schedule, 0)

	err := r.builder.
		Select(
			goqu.I("id"),
			goqu.I("duration"),
			goqu.I("event_id"),
			goqu.I("payload"),
			goqu.I("finishes_at"),
		).
		From(goqu.T("schedules")).
		Where(goqu.I("status").NotIn(STATUS_EXECUTED, STATUS_CANCELED)).
		Order(goqu.I("finishes_at").Asc()).
		ScanStructs(&schedules)

	if err != nil {
		return nil, err
	}

	return schedules, nil
}

func (r *goquRepository) UpdateStatus(scheduleId int64, status Status) error {
	_, err := r.builder.
		Update(goqu.T("schedules")).
		Set(goqu.Record{"status": status}).
		Where(goqu.I("id").Eq(scheduleId)).
		Executor().
		Exec()

	return err
}

func (r *goquRepository) CancelEventId(eventId any) error {
	_, err := r.builder.
		Update(goqu.T("schedules")).
		Set(goqu.Record{"status": STATUS_CANCELED}).
		Where(goqu.I("event_id").Eq(eventId)).
		Executor().
		Exec()

	return err
}

func (r *goquRepository) SaveSchedule(id any, finishesAt time.Time, payload []byte) (*Schedule, error) {
	schedule := &Schedule{
		EventId:    id,
		Payload:    payload,
		FinishesAt: &finishesAt,
		Status:     STATUS_PENDING,
	}

	return r.save(schedule)
}

func (r *goquRepository) SaveInterval(id any, duration time.Duration, payload []byte) (*Schedule, error) {
	schedule := &Schedule{
		EventId:  id,
		Payload:  payload,
		Duration: &duration,
		Status:   STATUS_PENDING,
	}

	return r.save(schedule)
}

func (r *goquRepository) save(schedule *Schedule) (*Schedule, error) {
	result, err := r.builder.
		Insert(goqu.T("schedules")).
		Rows(schedule).
		// needs not to interpolate since payload is binary
		Prepared(true).
		Executor().
		Exec()

	if err != nil {
		return nil, err
	}

	insertId, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	schedule.Id = insertId
	return schedule, nil
}
