package scheduler_test

import (
	"api/database"
	"api/scheduler"
	"testing"
	"time"
)

func TestRepository(t *testing.T) {
	conn, err := database.GetConnection(database.SQLITE, "../test.db")
	if err != nil {
		t.Fatalf("could not get connection: %s", err)
	}

	tx, err := conn.DB.Begin()
	if err != nil {
		t.Fatalf("could not start transaction: %s", err)
	}

	if _, err := tx.Exec(`
        INSERT INTO schedules (id, event_id, payload, status, finishes_at, duration) VALUES
        (1, 1, x'74657374', "PENDING", "2024-02-25 15:35:25", NULL),
        (2, 2, x'74657374', "PENDING", "2024-02-25 18:35:25", NULL),
        (3, 3, x'74657374', "PENDING", "2024-02-25 20:35:25", NULL),
        (4, 4, x'74657374', "EXECUTED", "2024-02-25 22:35:25", NULL),
        (5, 5, x'74657374', "CANCELED", "2024-02-25 23:35:25", NULL),
        (6, 6, x'74657374', "PENDING", NULL, 100000000)
    `); err != nil {
		t.Fatalf("could not seed schedules: %s", err)
	}

	if err := tx.Commit(); err != nil {
		t.Fatalf("could not commit transaction: %s", err)
	}

	t.Cleanup(func() {
		if _, err := conn.DB.Exec(`DELETE FROM schedules`); err != nil {
			t.Errorf("could not cleanup schedules: %s", err)
		}
	})

	repository := scheduler.NewRepository(conn)

	t.Run("LoadSchedules", func(t *testing.T) {
		t.Run("ignores executed and canceled", func(t *testing.T) {
			schedules, err := repository.LoadSchedules()
			if err != nil {
				t.Fatalf("could not load schedules: %s", err)
			}

			if len(schedules) != 4 {
				t.Errorf("expected %d schedules, got %d", 4, len(schedules))
			}
		})
	})

	t.Run("UpdateStatus", func(t *testing.T) {
		err := repository.UpdateStatus(1, scheduler.STATUS_EXECUTED)
		if err != nil {
			t.Fatalf("could not update status: %s", err)
		}

		schedules, err := repository.LoadSchedules()
		if err != nil {
			t.Fatalf("could not load schedules: %s", err)
		}

		if len(schedules) != 3 {
			t.Errorf("expected %d schedules, got %d", 3, len(schedules))
		}
	})

	t.Run("CancelEventId", func(t *testing.T) {
		if err := repository.CancelEventId(2); err != nil {
			t.Fatalf("could not cancel event id: %s", err)
		}

		schedules, err := repository.LoadSchedules()
		if err != nil {
			t.Fatalf("could not load schedules: %s", err)
		}

		if len(schedules) != 2 {
			t.Errorf("expected %d schedules, got %d", 2, len(schedules))
		}
	})

	t.Run("SaveSchedule", func(t *testing.T) {
		schedule, err := repository.SaveSchedule(69, time.Now().Add(time.Second), []byte("test"))
		if err != nil {
			t.Fatalf("could not save schedule: %s", err)
		}

		if schedule.Id == 0 {
			t.Error("should have set id")
		}

		if schedule.EventId.(int) != 69 {
			t.Errorf("expected event id %d, got %d", 69, schedule.EventId.(int))
		}

		if string(schedule.Payload) != "test" {
			t.Errorf("expected payload %s, got %s", "test", string(schedule.Payload))
		}

		schedules, err := repository.LoadSchedules()
		if err != nil {
			t.Fatalf("could not load schedules: %s", err)
		}

		if len(schedules) != 3 {
			t.Errorf("expected %d schedules, got %d", 3, len(schedules))
		}
	})
}
