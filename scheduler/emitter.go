package scheduler

import (
	"encoding/gob"
	"errors"
	"log"
	"reflect"
	"sync"
)

type (
	Emitter interface {
		Emit(payload any) error
		Subscribe(callback any) error
	}

	emitter struct {
		logger      *log.Logger
		subscribers sync.Map
	}
)

func NewEmitter(logger *log.Logger) Emitter {
	return &emitter{logger: logger}
}

func (e *emitter) Subscribe(callback any) error {
	typeOf := reflect.TypeOf(callback)

	if typeOf.Kind() != reflect.Func {
		return errors.New("callback is not a function")
	}

	if typeOf.NumIn() != 1 {
		return errors.New("callback should have only one parameter")
	}

	if typeOf.NumOut() != 1 {
		return errors.New("callback should have only one return")
	}

	errorInterface := reflect.TypeOf((*error)(nil)).Elem()
	if !typeOf.Out(0).Implements(errorInterface) {
		return errors.New("callback should return an error")
	}

	valueOf := reflect.New(typeOf.In(0))
	gob.Register(valueOf.Elem().Interface())

	// Uses the name of the parameter struct as the name of the event
	event := typeOf.In(0).Name()
	subscribers, _ := e.subscribers.LoadOrStore(event, []any{})
	e.subscribers.Store(event, append(subscribers.([]any), callback))

	return nil
}

func (e *emitter) Emit(data any) error {
	// Use the name of the payload as the name of the event
	event := reflect.TypeOf(data).Name()

	if subscribers, ok := e.subscribers.Load(event); ok {
		for _, callback := range subscribers.([]any) {
			// Invoke the callback
			output := reflect.ValueOf(callback).Call([]reflect.Value{
				reflect.ValueOf(data),
			})

			if err := output[0].Interface(); err != nil {
				return err.(error)
			}
		}
	}

	return nil
}
