package warehouse_test

import (
	"api/auth"
	"api/server"
	"api/warehouse"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestWarehouseService(t *testing.T) {
	t.Setenv(server.JWT_SECRET_KEY, "secret")

	token, err := auth.GenerateToken(1, "secret")
	if err != nil {
		t.Fatalf("could not generate token: %s", err)
	}

	svr := server.NewServer()
	svc := warehouse.NewService(warehouse.NewFakeRepository())
	warehouse.CreateEndpoints(svr, svc)

	t.Run("GetInventory", func(t *testing.T) {
		t.Run("should validate authentication", func(t *testing.T) {
			req := httptest.NewRequest("GET", "/warehouse/me", nil)
			req.Header.Set("Accept", "application/json")

			rec := httptest.NewRecorder()
			svr.ServeHTTP(rec, req)

			if rec.Code != http.StatusUnauthorized {
				t.Errorf("expected status %d, got %d", http.StatusUnauthorized, rec.Code)
			}
		})

		t.Run("should return authenticated company's inventory", func(t *testing.T) {
			req := httptest.NewRequest("GET", "/warehouse/me", nil)
			req.Header.Set("Accept", "application/json")
			req.Header.Set("Authorization", "Bearer "+token)

			rec := httptest.NewRecorder()
			svr.ServeHTTP(rec, req)

			if rec.Code != http.StatusOK {
				t.Errorf("expected status %d, got %d", http.StatusOK, rec.Code)
			}

			var response *warehouse.Inventory
			if err := json.Unmarshal(rec.Body.Bytes(), &response); err != nil {
				t.Fatalf("could not parse response: %s", err)
			}

			if len(response.Items) != 4 {
				t.Errorf("expected %d items, got %d", 4, len(response.Items))
			}
		})
	})

	t.Run("GetItem", func(t *testing.T) {
		t.Run("should validate authentication", func(t *testing.T) {
			req := httptest.NewRequest("GET", "/warehouse/me/1", nil)
			req.Header.Set("Accept", "application/json")

			rec := httptest.NewRecorder()
			svr.ServeHTTP(rec, req)

			if rec.Code != http.StatusUnauthorized {
				t.Errorf("expected status %d, got %d", http.StatusUnauthorized, rec.Code)
			}
		})

		t.Run("should validate resource", func(t *testing.T) {
			req := httptest.NewRequest("GET", "/warehouse/me/foobar", nil)
			req.Header.Set("Accept", "application/json")
			req.Header.Set("Authorization", "Bearer "+token)

			rec := httptest.NewRecorder()
			svr.ServeHTTP(rec, req)

			if rec.Code != http.StatusBadRequest {
				t.Errorf("expected status %d, got %d", http.StatusBadRequest, rec.Code)
			}
		})

		t.Run("should validate quality", func(t *testing.T) {
			req := httptest.NewRequest("GET", "/warehouse/me/1?quality=foobar", nil)
			req.Header.Set("Accept", "application/json")
			req.Header.Set("Authorization", "Bearer "+token)

			rec := httptest.NewRecorder()
			svr.ServeHTTP(rec, req)

			if rec.Code != http.StatusBadRequest {
				t.Errorf("expected status %d, got %d", http.StatusBadRequest, rec.Code)
			}
		})

		t.Run("should return inventory item", func(t *testing.T) {
			req := httptest.NewRequest("GET", "/warehouse/me/1?quality=0", nil)
			req.Header.Set("Accept", "application/json")
			req.Header.Set("Authorization", "Bearer "+token)

			rec := httptest.NewRecorder()
			svr.ServeHTTP(rec, req)

			if rec.Code != http.StatusOK {
				t.Errorf("expected status %d, got %d", http.StatusOK, rec.Code)
			}
		})

		t.Run("should return 404 if not found", func(t *testing.T) {
			req := httptest.NewRequest("GET", "/warehouse/me/3?quality=0", nil)
			req.Header.Set("Accept", "application/json")
			req.Header.Set("Authorization", "Bearer "+token)

			rec := httptest.NewRecorder()
			svr.ServeHTTP(rec, req)

			if rec.Code != http.StatusNotFound {
				t.Errorf("expected status %d, got %d", http.StatusNotFound, rec.Code)
			}
		})
	})
}
