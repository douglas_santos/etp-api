package warehouse

import (
	"api/auth"
	"errors"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func CreateEndpoints(e *echo.Echo, service Service) {
	group := e.Group("/warehouse")

	group.GET("/me", func(c echo.Context) error {
		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		resources, err := service.GetInventory(c.Request().Context(), companyId)
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, resources)
	})

	group.GET("/me/:resource", func(c echo.Context) error {
		resourceId, err := strconv.ParseInt(c.Param("resource"), 10, 64)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		quality, err := strconv.ParseInt(c.QueryParam("quality"), 10, 64)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		companyId, err := auth.GetIdFromToken(c.Get("user"))
		if err != nil {
			return err
		}

		resource, err := service.GetItem(c.Request().Context(), resourceId, quality, companyId)
		if err != nil {
			if errors.Is(err, ErrItemNotFound) {
				return echo.NewHTTPError(http.StatusNotFound)
			}
			return err
		}

		return c.JSON(http.StatusOK, resource)
	})
}
