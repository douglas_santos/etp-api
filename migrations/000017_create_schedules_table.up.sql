CREATE TABLE IF NOT EXISTS `schedules` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `event_id` VARCHAR(100),
    `event_type` VARCHAR(100),
    `payload` BLOB,
    `finishes_at` TIMESTAMP DEFAULT NULL,
    `duration` INTEGER DEFAULT NULL,
    `status` VARCHAR(50) DEFAULT 'PENDING'
);
