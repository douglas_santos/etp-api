DROP TABLE IF EXISTS `orders_purchases`;

CREATE TABLE IF NOT EXISTS `orders_transactions` (
    `order_id` INTEGER,
    `quantity` INTEGER,
    `transaction_id` INTEGER,
    PRIMARY KEY (`order_id`, `transaction_id`),
    FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
    FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
);
