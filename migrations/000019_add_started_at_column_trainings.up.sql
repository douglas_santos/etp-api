CREATE TABLE IF NOT EXISTS `trainings_2` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `result` TINYINT,
    `staff_id` INTEGER,
    `company_id` INTEGER,
    `investment` INTEGER,
    `finishes_at` TIMESTAMP,
    `completed_at` TIMESTAMP,
    `started_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (`staff_id`) REFERENCES `research_staff` (`id`),
    FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
);

INSERT INTO `trainings_2` (`id`, `result`, `staff_id`, `company_id`, `investment`, `finishes_at`, `completed_at`)
SELECT `id`, `result`, `staff_id`, `company_id`, `investment`, `finishes_at`, `completed_at`
FROM `trainings`;

DROP TABLE IF EXISTS `trainings`;

ALTER TABLE `trainings_2` RENAME TO `trainings`;
