CREATE TABLE IF NOT EXISTS `orders_purchases` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `order_id` INTEGER,
    `company_id` INTEGER,
    `quantity` INTEGER,
    `price` INTEGER,
    `profit` INTEGER,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
    FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
);

DROP TABLE IF EXISTS `orders_transactions`;
