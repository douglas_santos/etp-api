CREATE TABLE IF NOT EXISTS `bonds_2` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `company_id` INTEGER NOT NULL,
    `amount` INTEGER NOT NULL,
    `interest_rate` DECIMAL(4, 2) NOT NULL,
    `canceled_at` TIMESTAMP,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (`company_id`) REFERENCES `companies`(`id`)
);

INSERT INTO `bonds_2` (`id`, `company_id`, `amount`, `interest_rate`)
SELECT `id`, `company_id`, `amount`, `interest_rate`
FROM `bonds`;

DROP TABLE IF EXISTS `bonds`;

ALTER TABLE `bonds_2` RENAME TO `bonds`;
