CREATE TABLE IF NOT EXISTS `research_staff_2` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `salary` INTEGER UNSIGNED DEFAULT 0,
    `skill` TINYINT UNSIGNED DEFAULT 0,
    `talent` TINYINT UNSIGNED DEFAULT 0,
    `status` TINYINT UNSIGNED DEFAULT 0,
    `offer` INTEGER UNSIGNED DEFAULT 0,
    `company_id` INTEGER NOT NULL,
    `poacher_id` INTEGER DEFAULT NULL,
    `hired_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `on_strike_until` TIMESTAMP DEFAULT NULL,
    FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
    FOREIGN KEY (`poacher_id`) REFERENCES `companies` (`id`)
);

INSERT INTO `research_staff_2` (`id`, `name`, `salary`, `skill`, `talent`, `status`, `offer`, `company_id`, `poacher_id`)
SELECT `id`, `name`, `salary`, `skill`, `talent`, `status`, `offer`, `company_id`, `poacher_id`
FROM `research_staff`;

DROP TABLE IF EXISTS `research_staff`;

ALTER TABLE `research_staff_2` RENAME TO `research_staff`;
